$(function () {
    $("#ent_frete").mask('000.000.000.000,00', {reverse: true});
    $("#ent_imposto").mask('000.000.000.000,00', {reverse: true});
    $("#ent_total").mask('000.000.000.000,00', {reverse: true});
    $("#ent_num_nf").mask('00000000000000000');
    $("#iped_qtd").mask('00000000000000000');

    $("[name=formEntrada]").validate({
        rules: {

            trans_id: {
                required: true
            },

            ent_frete: {
                required: true
            },

            ent_num_nf: {
                required: true
            },

            ent_imposto: {
                required: true
            },

            ent_total: {
                required: true
            },

        },
        messages: {

            trans_id: {
                required: "O campo Transportadora é obrigatório.",
            },

            ent_frete: {
                required: "O campo Frete é obrigatório.",
            },

            ent_num_nf: {
                required: "O campo Nota Fiscal é obrigatório.",
            },

            ent_imposto: {
                required: "O campo Imposto é obrigatório.",
            },
            ent_total: {
                required: "O campo Total é obrigatório.",
            },

        },
        submitHandler: function (form) {
            var arr = new Array();
            $("#conteudoItem").find('tr').each(function () {
                arr.push({
                    ped_id:$(this.cells[0]).attr('data-pedido'),
                    prod_id: $(this.cells[0]).text(),
                    ient_qtd: $(this.cells[4]).text(),
                    ient_valor :$(this.cells[5]).find('input').val(),
                    ient_imposto:$(this.cells[6]).find('input').val()
                })
            });

            $('<input/>').attr({type: 'hidden', name: 'itens_entrada', value: JSON.stringify(arr)}).appendTo('#formEntrada');

            form.submit();
        },
    });


    $(".btnModal").on('click', function (e) {
        var ped_id = $(this).attr('data-pedido');

        $.ajax({
            type: "GET",
            url: url_base('pedidoCompra/pedidojson/' + ped_id),
            success: function (data) {
                $(".modal-body").append(getConteudo(JSON.parse(data)));
            }
        });
    });

    $("#btnAdd").on('click', function (e) {
        var produto, qtd;
        produto = $("#prod_id option:selected").attr('data-produto');
        qtd = $("#iped_qtd").val();
        if (typeof produto != 'undefined' && produto != '' && typeof qtd != 'undefined' && qtd != '') {
            produto = JSON.parse(produto);
            $("#conteudoItem").append(getConteudoItem(produto, qtd))
        } else {
            alert('escolha um produto e informe a quantidade')
        }
    })


    function getConteudoItem(produto, qtd) {
        return ('<tr><th scope="row">' + produto.prod_id + '</th>'
            + '<td>' + produto.prod_nome + '</td>'
            + '<td>' + produto.fabricante + '</td>'
            + '<td>' + produto.prod_marca + '</td>'
            + '<td>' + qtd + '</td>'
            + ' <td><input type="text"></td>'
            + ' <td><input type="text"></td>'
            + '<td>'
            + '<button type="button" onclick="excluir(this)"'
            + 'class="btn btn-default" aria-label="Left Align">'
            + '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>'
            + '</button>'
            + '</td></tr>');
    }

    function getConteudo(data) {
        return '<div class="form-group">' +
            '    <div class="col-lg-offset-1 col-sm-10">' +
            '        <div class="table-responsive">' +
            '            <table class="table table-striped table-bordered">' +
            '                <thead>' +
            '                <tr>' +
            '                    <th>ID</th>' +
            '                    <th>Usuário</th>' +
            '                    <th>Produto</th>' +
            '                    <th>Fabricante</th>' +
            '                    <th>Marca</th>' +
            '                    <th>Quantidade</th>' +
            '                    <th>Ação</th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody>' +
            getConteudoModal(data) +
            '                </tbody>' +
            '            </table>' +
            '        </div>' +
            '    </div>' +
            '</div>'
    }

    function getConteudoModal(data) {
        var row = '';
        data.itensPedido.forEach(function (value, key) {
            row += '<tr>' +
                '     <th scope="row"> ' + value.prod_id + '</th>' +
                '     <td> ' + data.usu_nome + '</td>' +
                '     <td> ' + value.prod_nome + '</td>' +
                '     <td> ' + value.fab_nome + '</td>' +
                '     <td> ' + value.prod_nome + '</td>' +
                '     <td> ' + value.iped_qtd + '</td>' +
                '     <td>' +
                '        <button type="button" onclick=\'inserir(this,' + JSON.stringify(value) + ')\'' +
                '           class="btn btn-default" aria-label="Left Align">' +
                '              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' +
                '        </button>' +
                '     </td>' +
                '</tr>'
        })
        return row;
    }

});

function inserir(e,produto) {
    $("#conteudoItem").append(
        '<tr>' +
        '   <th scope="row" data-pedido="'+produto.ped_id+'">' + produto.prod_id + '</th>'
        + ' <td>' + produto.prod_nome + '</td>'
        + ' <td>' + produto.fab_nome + '</td>'
        + ' <td>' + produto.prod_marca + '</td>'
        + ' <td>' + produto.iped_qtd + '</td>'
        + ' <td><input type="text" class="valor"></td>'
        + ' <td><input type="text" class="valor"></td>'
        + ' <td>'
        + ' <button type="button" onclick="excluir(this)"'
        + ' class="btn btn-default" aria-label="Left Align">'
        + ' <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>'
        + ' </button>'
        + ' </td></tr>'
    )
    $(".valor").mask('000.000.000.000,00', {reverse: true});
    excluir(e,produto);

}

function excluir(e,produto) {

    $(e).closest('tr').each(function () {
        this.remove();
    });
    $("#"+produto.ped_id).remove();
}
