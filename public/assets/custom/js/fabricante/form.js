$(function () {

    $('#for_cnpj').mask('00000000000000');
    $('#for_cep').mask('00000000');

    $("[name=formFabricante]").validate({
        rules: {

            fab_nome: {
                required: true
            },

            fab_cnpj: {
                required: true
            },

            cid_id: {
                required: true
            },

            fab_bairro: {
                required: true
            },

            fab_logradouro: {
                required: true
            },


            fab_numero: {
                required: true
            },

            fab_cep: {
                required: true
            },


        },
        messages: {

            fab_nome: {
                required: "O campo Nome � obrigat�rio.",
            },

            fab_cnpj: {
                required: "O campo CNPJ � obrigat�rio.",
            },

            cid_id: {
                required: "O campo cidade � obrigat�rio.",
            },
            fab_bairro: {
                required: "O campo Bairro � obrigat�rio.",
            },
            fab_logradouro: {
                required: "O campo Logradouro � obrigat�rio.",
            },
            fab_numero: {
                required: "O campo N�mero � obrigat�rio.",
            },
            fab_cep: {
                required: "O campo CEP � obrigat�rio.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });

})