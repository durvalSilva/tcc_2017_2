$(function () {
    $("#uf").on('change', function (e) {
        var uf = $(this).val();
        $.ajax({
            type: "GET",
            url: url_base('cidade/uf/' + uf),
            success: function (data) {
                data = JSON.parse(data);
                $("#cid_id").empty();
                $("#cid_id").append("<option value='' selected>Selecione...</option>");

                data.forEach(function (v, k) {
                    $("#cid_id").append("<option value='" + v.cid_id + "'>" + v.cid_nome + "</option>");
                })
            }
        });
    });
});