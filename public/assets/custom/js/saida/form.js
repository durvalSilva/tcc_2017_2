$(function () {
    $("#sai_frete").mask('000.000.000.000,00', {reverse: true});
    $("#sai_imposto").mask('000.000.000.000,00', {reverse: true});
    $("#sai_total").mask('000.000.000.000,00', {reverse: true});
    $("#iped_qtd").mask('00000000000000000');

    $("[name=formSaida]").validate({
        rules: {

            trans_id: {
                required: true
            },

            loj_id: {
                required: true
            },

            sai_frete: {
                required: true
            },

            sai_imposto: {
                required: true
            },

            sai_total: {
                required: true
            },

        },
        messages: {

            trans_id: {
                required: "O campo Transportadora é obrigatório.",
            },

            loj_id: {
                required: "O campo Loja é obrigatório.",
            },

            sai_frete: {
                required: "O campo Frete é obrigatório.",
            },

            sai_imposto: {
                required: "O campo Imposto é obrigatório.",
            },
            sai_total: {
                required: "O campo Total é obrigatório.",
            },

        },
        submitHandler: function (form) {

            var arr = new Array();
            $("#conteudoItem").find('tr').each(function () {
                arr.push({
                    prod_id: $(this.cells[0]).text(),
                    isai_qtd: $(this.cells[4]).text(),
                    isai_valor: 10,
                    isai_imposto: 10
                })
            });

            $('<input/>').attr({
                type: 'hidden',
                name: 'itens_saida',
                value: JSON.stringify(arr)
            }).appendTo('#formSaida');

            form.submit();
        },
    });



    $("#btnAdd").on('click', function (e) {
        var produto, qtd;
        produto = $("#prod_id option:selected").attr('data-produto');
        qtd = $("#iped_qtd").val();
        if (typeof produto != 'undefined' && produto != '' && typeof qtd != 'undefined' && qtd != '') {
            produto = JSON.parse(produto);
            $("#conteudoItem").append(getConteudoItem(produto, qtd))
        } else {
            alert('escolha um produto e informe a quantidade')
        }
    })


    function getConteudoItem(produto, qtd) {
        return ('<tr><th scope="row">' + produto.prod_id + '</th>'
            + '<td>' + produto.prod_nome + '</td>'
            + '<td>' + produto.fabricante + '</td>'
            + '<td>' + produto.prod_marca + '</td>'
            + '<td>' + qtd + '</td>'
            + '<td>'
            + '<button type="button" onclick="excluir(this)"'
            + 'class="btn btn-default" aria-label="Left Align">'
            + '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>'
            + '</button>'
            + '</td></tr>');
    }

});


function excluir(e) {

    $(e).closest('tr').each(function () {
        this.remove();
    });
}
