$(function () {
    $('#trans_cnpj').mask('00000000000000');
    $('#trans_cep').mask('00000000');


    $("[name=formTransportadora]").validate({
        rules: {

            trans_nome: {
                required: true
            },

            trans_cnpj: {
                required: true
            },

            cid_id: {
                required: true
            },

            trans_bairro: {
                required: true
            },

            trans_logradouro: {
                required: true
            },


            trans_numero: {
                required: true
            },

            trans_cep: {
                required: true
            },


        },
        messages: {

            trans_nome: {
                required: "O campo Nome é obrigatório.",
            },

            trans_cnpj: {
                required: "O campo CNPJ é obrigatório.",
            },

            cid_id: {
                required: "O campo cidade é obrigatório.",
            },

            trans_bairro: {
                required: "O campo Bairro é obrigatório.",
            },
            trans_logradouro: {
                required: "O campo Logradouro é obrigatório.",
            },
            trans_numero: {
                required: "O campo Número é obrigatório.",
            },
            trans_cep: {
                required: "O campo CEP é obrigatório.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });
});