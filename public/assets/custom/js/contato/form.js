$(function () {
    $("#con_fone").mask('(00) 00000-0000');

    $("[name=formContato]").validate({
        rules: {

            con_nome: {
                required: true
            },

            con_cargo: {
                required: true
            },

            con_fone: {
                required: true
            },

        },
        messages: {

            con_nome: {
                required: "O campo Nome é obrigatório.",
            },

            con_cargo: {
                required: "O campo Cargo é obrigatório.",
            },

            con_fone: {
                required: "O campo Fone é obrigatório.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });
    
});