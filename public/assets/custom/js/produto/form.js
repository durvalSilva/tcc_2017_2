$(function () {
    $("#prod_qtd_min").mask('00000000000000000');
    $("[name=formProduto]").validate({
        rules: {

            fab_id: {
                required: true
            },

            prod_nome: {
                required: true
            },

            pcat_id: {
                required: true
            },

            prod_marca: {
                required: true
            },

            prod_qtd_min: {
                required: true
            },

            prod_descricao: {
                required: true
            },
        },
        messages: {

            fab_id: {
                required: "O campo Fabricante é obrigatório.",
            },

            prod_nome: {
                required: "O campo Produto é obrigatório.",
            },

            pcat_id: {
                required: "O campo Categoria é obrigatório.",
            },

            prod_marca: {
                required: "O campo Marca é obrigatório.",
            },
            prod_qtd_min: {
                required: "O campo Quantidade minima é obrigatório.",
            },
            prod_descricao: {
                required: "O campo Descrição é obrigatório.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    })
})