$(function () {

    $('#for_cnpj').mask('00000000000000');
    $('#for_insc').mask('00000000000000');
    $('#for_cep').mask('00000000');



    $("[name=formTransportadora]").validate({
        rules: {

            trans_nome: {
                required: true
            },

            trans_cnpj: {
                required: true
            },

            cid_id: {
                required: true
            },

            trans_bairro: {
                required: true
            },

            trans_logradouro: {
                required: true
            },


            trans_numero: {
                required: true
            },

            trans_cep: {
                required: true
            },


        },
        messages: {

            trans_nome: {
                required: "O campo Nome � obrigat�rio.",
            },

            trans_cnpj: {
                required: "O campo CNPJ � obrigat�rio.",
            },

            cid_id: {
                required: "O campo cidade � obrigat�rio.",
            },

            trans_bairro: {
                required: "O campo Bairro � obrigat�rio.",
            },
            trans_logradouro: {
                required: "O campo Logradouro � obrigat�rio.",
            },
            trans_numero: {
                required: "O campo N�mero � obrigat�rio.",
            },
            trans_cep: {
                required: "O campo CEP � obrigat�rio.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });

});