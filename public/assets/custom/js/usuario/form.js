$(function () {

    $("[name=formUsuario]").validate({
        rules: {

            usu_nome: {
                required: true
            },

            tusu_id: {
                required: true
            },

            usu_email: {
                required: true
            },

            usu_senha: {
                required: true
            },

        },
        messages: {

            usu_nome: {
                required: "O campo Nome é obrigatório.",
            },

            tusu_id: {
                required: "O campo Tipo é obrigatório.",
            },

            usu_email: {
                required: "O campo e-mail é obrigatório.",
            },

            usu_senha: {
                required: "O campo senha é obrigatório.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });

});