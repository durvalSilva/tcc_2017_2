$(function () {

    $('#for_cnpj').mask('00000000000000');
    $('#loj_insc').mask('00000000000000');
    $('#for_cep').mask('00000000');

    $("[name=formLoja]").validate({
        rules: {

            loj_nome: {
                required: true
            },

            loj_cnpj: {
                required: true
            },

            loj_insc: {
                required: true
            },

            cid_id: {
                required: true
            },

            loj_bairro: {
                required: true
            },

            loj_logradouro: {
                required: true
            },

            loj_numero: {
                required: true
            },

            loj_cep: {
                required: true
            },


        },
        messages: {

            fab_nome: {
                required: "O campo Nome é obrigatório.",
            },

            fab_cnpj: {
                required: "O campo CNPJ é obrigatório.",
            },

            loj_insc: {
                required: "O campo Ins.Estadual é obrigatório.",
            },

            cid_id: {
                required: "O campo cidade é obrigatório.",
            },
            fab_bairro: {
                required: "O campo Bairro é obrigatório.",
            },
            fab_logradouro: {
                required: "O campo Logradouro é obrigatório.",
            },
            fab_numero: {
                required: "O campo Número é obrigatório.",
            },
            fab_cep: {
                required: "O campo CEP é obrigatório.",
            },

        },
        submitHandler: function (form) {
            // $.blockUI({message: '<img src=' + base_url("public/img/spinner.gif") + ' width="80" align="middle" /> Aguarde...'});
            //$.unblockUI();
            form.submit();
        },
    });
});