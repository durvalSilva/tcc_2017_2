$(function () {
    $("#iped_qtd").mask('00000000000000');

    $("#btnAdd").on('click', function (e) {
        var produto, qtd;
        produto = $("#prod_id option:selected").attr('data-produto');
        qtd = $("#iped_qtd").val();
        if (typeof produto != 'undefined' && produto != '' && typeof qtd != 'undefined' && qtd != '') {
            produto = JSON.parse(produto);
            $("#conteudoItem").append(getConteudoItem(produto, qtd))
        } else {
            alert('escolha um produto e informe a quantidade')
        }
    })


    function getConteudoItem(produto, qtd) {
        return ('<tr><th scope="row">' + produto.prod_id + '</th>'
        + '<td>' + produto.prod_nome + '</td>'
        + '<td>' + produto.fabricante + '</td>'
        + '<td>' + produto.prod_marca + '</td>'
        + '<td>' + qtd + '</td>'
        + '<td>'
        + '<button type="button" onclick="excluir(this)"'
        + 'class="btn btn-default" aria-label="Left Align">'
        + '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>'
        + '</button>'
        + '</td></tr>');
    }


    $("#formPedido").submit(function (e) {
        var arr = new Array();
        $("#conteudoItem").find('tr').each(function () {
            arr.push({
                prod_id: $(this.cells[0]).text(),
                iped_qtd: $(this.cells[4]).text()
            })
        });

        $('<input/>').attr({type: 'hidden', name: 'itens_pedido', value: JSON.stringify(arr)}).appendTo('#formPedido');

        document.formPedido.submit();
    })
});

function excluir(e) {
    $(e).closest('tr').each(function () {
        this.remove();
    });
}