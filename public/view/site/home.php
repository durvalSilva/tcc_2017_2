<?php session_destroy();?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?=$this->title?></title>

<!--    <link rel="icon" href="../../favicon.ico">-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/assets/custom/css/login.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<body>

<div class="container">
    <form class="form-signin" action="autenticar" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" name="usu_email" id="usu_email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Senha</label>
            <input type="password" class="form-control" name="usu_senha" id="usu_senha" placeholder="Senha" required>
        </div>
<!--        -->
<!--        <div class="checkbox">-->
<!--            <label>-->
<!--                <input type="checkbox"> Check me out-->
<!--            </label>-->
<!--        </div>-->
        <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
    </form>

</body>


<!-- jQuery -->
<script src="public/assets/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="public/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>