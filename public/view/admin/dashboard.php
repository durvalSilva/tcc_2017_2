<!DOCTYPE html>
<html lang="pt-br">
<?php require_once('template/head.php')?>
<body>
<?php require_once('template/topMenu.php')?>
<?php require_once('template/sideMenu.php')?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<h3>Produto em Estoque</h3>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Produto</th>
                <th>Categoria</th>
                <th>Quantidade</th>
                <th>Marca</th>
                <th>Fabricante</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $item) { ?>
                <tr>
                    <th scope="row"><?= $item->prod_id ?></th>
                    <td><?= $item->prod_nome ?></td>
                    <td><?= $item->pcat_nome ?></td>
                    <td><?= $item->qtd_estoque ?></td>
                    <td><?= $item->prod_marca ?></td>
                    <td><?= $item->fab_nome ?></td>
                </tr>

            <?php } ?>
            </tbody>

        </table>
    </div>
</div>

<?php require_once('template/footer.php')?>
</body>
</html>
