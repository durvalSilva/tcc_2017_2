<?php
if (!empty($_SESSION['flashdata'])) { ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="alert <?= $_SESSION['flashdata']['status'] ?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <?php
                if (is_array($_SESSION['flashdata']['mensagem'])) {
                    ?>
                    <ul>
                        <?php
                        foreach ($_SESSION['flashdata']['mensagem'] as $item) { ?>
                            <li><?= $item ?></li>
                        <?php } ?>
                    </ul>
                <?php } else {
                    echo $_SESSION['flashdata']['mensagem'];
                }
                ?>
            </div>
            <!--            <div class="alert alert-info alert-dismissible" role="alert">-->
            <!--                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
            <!--                <strong>Heads up!</strong> This alert needs your attention, but-->
            <!--                it's not super important.-->
            <!--            </div>-->
            <!--            <div class="alert alert-warning alert-dismissible" role="alert">-->
            <!--                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
            <!--                <strong>Warning!</strong> Better check yourself, you're not-->
            <!--                looking too good.-->
            <!--            </div>-->
            <!--                <div class="alert alert-danger alert-dismissible" role="alert">-->
            <!--                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span-->
            <!--                            aria-hidden="true">&times;</span></button>-->
            <!--                    <strong>Oh snap!</strong> Change a few things up and try-->
            <!--                    submitting again.-->
            <!--                </div>-->
        </div>
    </div>
    <?php
    unset($_SESSION['flashdata']);
}
?>