<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <!--                <li class="active"><a href="#">Dashboard</span></a></li>-->
                <li><a href="<?= url_base('dashboard') ?>">Dashboard</span></a></li>
            </ul>
            <ul class="nav nav-sidebar">
                <li><a href="<?= url_base('transportadora') ?>">Transportadora</a></li>
                <li><a href="<?= url_base('fornecedor') ?>">Fornecedor</a></li>
                <li><a href="<?= url_base('fabricante') ?>">Fabricante</a></li>
                <li><a href="<?= url_base('loja') ?>">Loja</a></li>
                <li><a href="<?= url_base('contato') ?>">Contato</a></li>
            </ul>
            <ul class="nav nav-sidebar">
                <li><a href="<?= url_base('produto') ?>">Produto</a></li>
                <li><a href="<?= url_base('produtoCategoria') ?>">Produto Categoria</a></li>
                <li><a href="<?= url_base('entradaProduto') ?>">Entrada de Produto</a></li>
                <li><a href="<?= url_base('saidaProduto') ?>">Saida de Produto</a></li>
                <li><a href="<?= url_base('pedidoCompra') ?>">Pedido de Compra</a></li>
            </ul>
            <?php
            if ($_SESSION['usuario']->tipo->tusu_id == 1) {
                ?>
                <ul class="nav nav-sidebar">
                    <li><a href="<?= url_base('usuario') ?>">Usuário</a></li>
                </ul>
            <?php }
            ?>
        </div>
    </div>
</div>