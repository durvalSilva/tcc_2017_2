<script src="<?= url_base('public/assets/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= url_base('public/assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= url_base('public/assets/plugins/mask/jquery.mask.min.js') ?>"></script>
<script src="<?= url_base('public/assets/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?= url_base('public/assets/custom/js/sistema.js') ?>"></script>

<script>
    function url_base(caminho = '') {
        return '<?=url_base()?>' + caminho
    }
</script>
