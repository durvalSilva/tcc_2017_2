<div class="form-group">
    <label for="trans_nome" class="col-sm-1 control-label">Nome</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_nome" name="trans_nome" placeholder="Transportadora" required
               value="<?= isset($data) ? $data->trans_nome != null ? $data->trans_nome : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="trans_cnpj" class="col-sm-1 control-label">CNPJ</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_cnpj" name="trans_cnpj" placeholder="CNPJ" maxlength=14
               required
               value="<?= isset($data) ? $data->trans_cnpj != null ? $data->trans_cnpj : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="uf" class="col-sm-1 control-label">UF</label>

    <div class="col-sm-3">
        <select class="form-control" id="uf">
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                $cidade = $data->cidade();
                foreach ($estados as $item) {
                    if ($item->uf_id == $cidade->uf_id) {
                        ?>
                        <option value="<?= $item->uf_id ?>"
                                selected><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php }
                }
            } else {
                foreach ($estados as $item) { ?>
                    <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                <?php }
            } ?>
        </select>
    </div>
    <label for="cid_id" class="col-sm-1 control-label">Cidade</label>

    <div class="col-sm-6">
        <select class="form-control" id="cid_id" name="cid_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($cidades as $item) {
                    if ($data->cid_id == $item->cid_id) {
                        ?>
                        <option value="<?= $item->cid_id ?>" selected><?= utf8_encode($item->cid_nome) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                    <?php }
                }
            } else {
                foreach ($cidades as $item) { ?>
                    <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="trans_bairro" class="col-sm-1 control-label">Bairro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_bairro" name="trans_bairro" placeholder="Bairro" required
               value="<?= isset($data) ? $data->trans_bairro != null ? $data->trans_bairro : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="trans_logradouro" class="col-sm-1 control-label">Logradouro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_logradouro" name="trans_logradouro" placeholder="Logradouro"
               required
               value="<?= isset($data) ? $data->trans_logradouro != null ? $data->trans_logradouro : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="trans_numero" class="col-sm-1 control-label">Número</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_numero" name="trans_numero" placeholder="Número" required
               value="<?= isset($data) ? $data->trans_numero != null ? $data->trans_numero : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="trans_cep" class="col-sm-1 control-label">CEP</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="trans_cep" name="trans_cep" placeholder="CEP" maxlength=8 required
               value="<?= isset($data) ? $data->trans_cep != null ? $data->trans_cep : '' : '' ?>">
    </div>
</div>

