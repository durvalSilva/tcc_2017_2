<div class="form-group">
    <label for="fab_nome" class="col-sm-1 control-label">Nome</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_nome" name="fab_nome" placeholder="Fabricante" required
               value="<?= isset($data) ? $data->fab_nome != null ? $data->fab_nome : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="fab_cnpj" class="col-sm-1 control-label">CNPJ</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_cnpj" name="fab_cnpj" placeholder="CNPJ" maxlength=14 required
               value="<?= isset($data) ? $data->fab_cnpj != null ? $data->fab_cnpj : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="uf" class="col-sm-1 control-label">UF</label>

    <div class="col-sm-3">
        <select class="form-control" id="uf">
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                $cidade = $data->cidade();
                foreach ($estados as $item) {
                    if ($item->uf_id == $cidade->uf_id) {
                        ?>
                        <option value="<?= $item->uf_id ?>" selected><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php }
                }
            } else {
                foreach ($estados as $item) { ?>
                    <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                <?php }
            } ?>
        </select>
    </div>
    <label for="cid_id" class="col-sm-1 control-label">Cidade</label>

    <div class="col-sm-6">
        <select class="form-control" id="cid_id" name="cid_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($cidades as $item) {
                    if ($data->cid_id == $item->cid_id) {
                        ?>
                        <option value="<?= $item->cid_id ?>" selected><?= utf8_encode($item->cid_nome) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                    <?php }
                }
            } else {
                foreach ($cidades as $item) { ?>
                    <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="fab_bairro" class="col-sm-1 control-label">Bairro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_bairro" name="fab_bairro" placeholder="Bairro" required
               value="<?= isset($data) ? $data->fab_bairro != null ? $data->fab_bairro : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="fab_logradouro" class="col-sm-1 control-label">Logradouro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_logradouro" name="fab_logradouro" placeholder="Logradouro"
               required
               value="<?= isset($data) ? $data->fab_logradouro != null ? $data->fab_logradouro : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="fab_numero" class="col-sm-1 control-label">Número</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_numero" name="fab_numero" placeholder="Número" required
               value="<?= isset($data) ? $data->fab_numero != null ? $data->fab_numero : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="fab_cep" class="col-sm-1 control-label">CEP</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="fab_cep" name="fab_cep" placeholder="CEP" maxlength=8 required
               value="<?= isset($data) ? $data->fab_cep != null ? $data->fab_cep : '' : '' ?>">
    </div>
</div>

