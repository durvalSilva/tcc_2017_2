<div class="form-group">
    <label for="usu_nome" class="col-sm-1 control-label">Nome</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="usu_nome" name="usu_nome" placeholder="Nome" required
               value="<?= isset($data) ? $data->usu_nome != null ? $data->usu_nome : '' : ''  ?>">
    </div>
</div>

<div class="form-group">
    <label for="tusu_id" class="col-sm-1 control-label">Tipo</label>

    <div class="col-sm-10">
        <select class="form-control" id="tusu_id" name="tusu_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($tipos as $item) {
                    if ($data->tusu_id == $item->tusu_id) {
                        ?>
                        <option value="<?= $item->tusu_id ?>" selected><?= utf8_encode($item->tusu_nome) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->tusu_id ?>"><?= utf8_encode($item->tusu_nome) ?></option>
                    <?php }
                }
            } else {
                foreach ($tipos as $item) { ?>
                    <option value="<?= $item->tusu_id ?>"><?= utf8_encode($item->tusu_nome) ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="usu_email" class="col-sm-1 control-label">E-mail</label>

    <div class="col-sm-10">
        <input type="email" class="form-control" id="usu_email" name="usu_email" placeholder="E-mail" required
               value="<?= isset($data) ? $data->usu_email != null ? $data->usu_email : '' : ''  ?>">
    </div>
</div>


<div class="form-group">
    <label for="usu_senha" class="col-sm-1 control-label">Senha</label>

    <div class="col-sm-4">
        <input type="password" class="form-control" id="usu_senha" name="usu_senha" placeholder="Senha" minlength=6  required
               value="<?= isset($data) ? $data->usu_senha != null ? '******' : '' : '' ?>">
    </div>
    <label for="confirma_senha" class="col-sm-2 control-label">Confirmar Senha</label>

    <div class="col-sm-4">
        <input type="password" class="form-control" id="confirma_senha" placeholder="Senha" minlength=6  required
               value="<?= isset($data) ? $data->usu_senha != null ? '******' : '' : ''  ?>">
    </div>
</div>



