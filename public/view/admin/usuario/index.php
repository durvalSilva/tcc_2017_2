<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header" style="    display: inline-flex;width: 100%;">
        <div class="col-lg-12 col-md-12">
            <div class="col-lg-2 col-md-1">
                <h1>Usuários</h1>

            </div>
            <div class="col-lg-1 col-md-2 col-md-offset-9 col-lg-offset-9">
                <a type="button" class="btn btn-success btn-lg btn-block" href="usuario/create">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-top: 20%;"></span> Add
                </a>
            </div>
        </div>
    </div>
    <?php require_once('public/view/admin/template/mensagem.php') ?>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Tipo</th>
                <th>Data Criação</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $item) { ?>
                <tr>
                    <th scope="row"><?= $item->usu_id ?></th>
                    <td><?= $item->usu_nome ?></td>
                    <td><?= $item->usu_email ?></td>
                    <td><?= $item->tipoUsuario()->tusu_nome ?></td>
                    <td><?= date("d/m/Y H:i", strtotime($item->created_at)) ?></td>
                    <td>
                        <a href="<?= url_base('usuario/edit/' . $item->usu_id) ?>" type="button"
                           class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        <a href="<?= url_base('usuario/delete/' . $item->usu_id) ?>" type="button"
                           class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>

<?php require_once('public/view/admin/template/footer.php') ?>
</body>
</html>
