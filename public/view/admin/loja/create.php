<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header">
        <h1>Criar Loja</h1>
    </div>
    <?php require_once('public/view/admin/template/mensagem.php') ?>
    <div class="row">
        <form class="form-horizontal" action="<?= url_base('loja/store') ?>" method="POST" id="formLoja" name="formLoja">
            <?php require_once('public/view/admin/loja/_form.php') ?>
            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Cadastrar</button>
                </div>
            </div>
        </form>
    </div>

</div>

<?php require_once('public/view/admin/template/footer.php') ?>
<script src="<?=url_base('public/assets/custom/js/loja/form.js')?>"></script>
</body>
</html>
