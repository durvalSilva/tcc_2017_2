<div class="form-group">
    <label for="loj_nome" class="col-sm-1 control-label">Nome</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_nome" name="loj_nome" placeholder="loja" required
               value="<?= isset($data) ? $data->loj_nome != null ? $data->loj_nome : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="loj_cnpj" class="col-sm-1 control-label">CNPJ</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_cnpj" name="loj_cnpj" placeholder="CNPJ" maxlength=14
               required
               value="<?= isset($data) ? $data->loj_cnpj != null ? $data->loj_cnpj : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="loj_insc" class="col-sm-1 control-label">Ins.Estadual</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_insc" name="loj_insc" placeholder="Ins. Estadual" required
               value="<?= isset($data) ? $data->loj_insc != null ? $data->loj_insc : '' : '' ?>">
    </div>
</div>


<div class="form-group">
    <label for="uf" class="col-sm-1 control-label">UF</label>

    <div class="col-sm-3">
        <select class="form-control" id="uf">
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                $cidade = $data->cidade();
                foreach ($estados as $item) {
                    if ($item->uf_id == $cidade->uf_id) {
                        ?>
                        <option value="<?= $item->uf_id ?>" selected><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php }
                }
            } else {
                foreach ($estados as $item) { ?>
                    <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                <?php }
            } ?>
        </select>
    </div>
    <label for="cid_id" class="col-sm-1 control-label">Cidade</label>

    <div class="col-sm-6">
        <select class="form-control" id="cid_id" name="cid_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($cidades as $item) {
                    if ($data->cid_id == $item->cid_id) {
                        ?>
                        <option value="<?= $item->cid_id ?>" selected><?= utf8_encode($item->cid_nome) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                    <?php }
                }
            } else {
                foreach ($cidades as $item) { ?>
                    <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="loj_bairro" class="col-sm-1 control-label">Bairro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_bairro" name="loj_bairro" placeholder="Bairro" required
               value="<?= isset($data) ? $data->loj_bairro != null ? $data->loj_bairro : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="loj_logradouro" class="col-sm-1 control-label">Logradouro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_logradouro" name="loj_logradouro" placeholder="Logradouro"
               required
               value="<?= isset($data) ? $data->loj_logradouro != null ? $data->loj_logradouro : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="loj_numero" class="col-sm-1 control-label">Número</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_numero" name="loj_numero" placeholder="Número" required
               value="<?= isset($data) ? $data->loj_numero != null ? $data->loj_numero : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="loj_cep" class="col-sm-1 control-label">CEP</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="loj_cep" name="loj_cep" placeholder="CEP" maxlength=8 required
               value="<?= isset($data) ? $data->loj_cep != null ? $data->loj_cep : '' : '' ?>">
    </div>
</div>

