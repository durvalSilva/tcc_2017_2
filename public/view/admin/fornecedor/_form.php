<div class="form-group">
    <label for="for_nome" class="col-sm-1 control-label">Nome</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_nome" name="for_nome" placeholder="Fornecedor" required
               value="<?= isset($data) ? $data->for_nome != null ? $data->for_nome : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="for_cnpj" class="col-sm-1 control-label">CNPJ</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_cnpj" name="for_cnpj" placeholder="CNPJ" maxlength=14 required
               value="<?= isset($data) ? $data->for_cnpj != null ? $data->for_cnpj : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="for_rsocial" class="col-sm-1 control-label">Razão Social</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_rsocial" name="for_rsocial" placeholder="Razão Social" required
               value="<?= isset($data) ? $data->for_rsocial != null ? $data->for_rsocial : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="for_insc" class="col-sm-1 control-label">Ins.Estadual</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_insc" name="for_insc" placeholder="Ins.Estadual" required
               value="<?= isset($data) ? $data->for_insc != null ? $data->for_insc : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="uf" class="col-sm-1 control-label">UF</label>

    <div class="col-sm-3">
        <select class="form-control" id="uf" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                $cidade = $data->cidade();
                foreach ($estados as $item) {
                    if ($item->uf_id == $cidade->uf_id) {
                        ?>
                        <option value="<?= $item->uf_id ?>" selected><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                    <?php }
                }
            } else {
                foreach ($estados as $item) { ?>
                    <option value="<?= $item->uf_id ?>"><?= $item->uf_sigla . ' - ' . utf8_encode($item->uf_estado) ?></option>
                <?php }
            } ?>
        </select>
    </div>
    <label for="cid_id" class="col-sm-1 control-label">Cidade</label>

    <div class="col-sm-6">
        <select class="form-control" id="cid_id" name="cid_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($cidades as $item) {
                    if ($data->cid_id == $item->cid_id) {
                        ?>
                        <option value="<?= $item->cid_id ?>" selected><?= utf8_encode($item->cid_nome) ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->cid_id ?>"><?= utf8_encode($item->cid_nome) ?></option>
                    <?php }
                }
            } else {
                foreach ($cidades as $item) { ?>
                    <option value="<?= $item->cid_id ?>"><?=utf8_encode( $item->cid_nome) ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="for_bairro" class="col-sm-1 control-label">Bairro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_bairro" name="for_bairro" placeholder="Bairro" required
               value="<?= isset($data) ? $data->for_bairro != null ? $data->for_bairro : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="for_logradouro" class="col-sm-1 control-label">Logradouro</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_logradouro" name="for_logradouro" placeholder="Logradouro"
               required
               value="<?= isset($data) ? $data->for_logradouro != null ? $data->for_logradouro : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="for_numero" class="col-sm-1 control-label">Número</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_numero" name="for_numero" placeholder="Número" required
               value="<?= isset($data) ? $data->for_numero != null ? $data->for_numero : '' : '' ?>">
    </div>
</div>
<div class="form-group">
    <label for="fab_cep" class="col-sm-1 control-label">CEP</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="for_cep" name="for_cep" placeholder="CEP" maxlength=8 required
               value="<?= isset($data) ? $data->for_cep != null ? $data->for_cep : '' : '' ?>">
    </div>
</div>

