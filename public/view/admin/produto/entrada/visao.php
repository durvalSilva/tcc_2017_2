<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header">
        <h1>Entrada</h1>
    </div>
    <div class="row">
        <div style="display: inline-grid;font-size: 2rem;">
            <span style="margin-bottom: 10px;"><strong>Usuario:</strong> <?= $data->usuario()->usu_nome ?></span>
            <span style="margin-bottom: 10px;"><strong>Transportadora:</strong> <?= $data->transportadora()->trans_nome ?></span>
            <span style="margin-bottom: 10px;"><strong>Nota Fiscal:</strong> <?= $data->ent_num_nf ?></span>
            <span style="margin-bottom: 10px;"><strong>Total:</strong> <?= $data->ent_total ?></span>
            <span style="margin-bottom: 10px;"><strong>Frete:</strong> <?= $data->ent_frete ?></span>
            <span style="margin-bottom: 10px;"><strong>Imposto:</strong> <?= $data->ent_imposto ?></span>
        </div>
    </div>
    <div class="row">
        <h2>Itens Entrada</h2>

        <?php
        foreach ($data->itemEntrada() as $item) {
            ?>
            <div class="col-sm-3" style="display: inline-grid;font-size: 2rem;">
                <span style="margin-bottom: 10px;"><strong>Produto:</strong> <?= $item->produto()->prod_nome ?></span>
                <span style="margin-bottom: 10px;"><strong>Quantidade:</strong> <?= $item->ient_qtd ?></span>
                <span style="margin-bottom: 10px;"><strong>Valor:</strong> <?= $item->ient_valor ?></span>
                <span style="margin-bottom: 10px;"><strong>Imposto:</strong> <?= $item->ient_imposto ?></span>
            </div>
        <?php }

        ?>
    </div>

</div>

<?php require_once('public/view/admin/template/footer.php') ?>

</body>
</html>
