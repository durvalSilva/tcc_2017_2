<div class="form-group">
    <label for="trans_id" class="col-sm-1 control-label">Transportadora</label>

    <div class="col-sm-10">
        <select class="form-control" id="trans_id" name="trans_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($transportadoras as $item) {
                    if ($data->trans_id == $item->trans_id) {
                        ?>
                        <option value="<?= $item->trans_id ?>"
                                selected><?= $item->trans_nome ?></option>
                    <?php } else { ?>
                        <option value="<?= $item->trans_id ?>"><?= $item->trans_nome ?></option>
                    <?php }
                }
            } else {
                foreach ($transportadoras as $item) { ?>
                    <option value="<?= $item->trans_id ?>"><?= $item->trans_nome ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="ent_frete" class="col-sm-1 control-label">Valor do Frete</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="ent_frete" name="ent_frete" placeholder="Frete" required
               value="<?= isset($data) ? $data->ent_frete != null ? $data->ent_frete : '' : ''  ?>">
    </div>
</div>

<div class="form-group">
    <label for="ent_num_nf" class="col-sm-1 control-label">Nota Fiscal</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="ent_num_nf" name="ent_num_nf" placeholder="Nota Fiscal"
               required
               value="<?= isset($data) ? $data->ent_num_nf != null ? $data->ent_num_nf : '' : ''  ?>">
    </div>
</div>
<div class="form-group">
    <label for="ent_imposto" class="col-sm-1 control-label">Valor do Imposto</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="ent_imposto" name="ent_imposto" placeholder="Imposto" required
               value="<?= isset($data) ? $data->ent_imposto != null ? $data->ent_imposto : '' : ''  ?>">
    </div>
</div>

<div class="form-group">

    <label for="cid_id" class="col-sm-1 control-label">Produto</label>

    <div class="col-sm-5">
        <select class="form-control" id="prod_id">
            <option value="" selected>Selecione...</option>
            <?php
            foreach ($produtos as $item) {
                $arrProd = json_encode(array(
                    'prod_id' => $item->prod_id,
                    'prod_nome' => utf8_encode($item->prod_nome),
                    'fabricante' => utf8_encode($item->fabricante()->fab_nome),
                    'prod_marca' => utf8_encode($item->prod_marca)
                ))
                ?>
                <option
                        value="<?= $item->prod_id ?>"
                        data-produto='<?= $arrProd ?>'><?= $item->prod_id . ' - ' . utf8_encode($item->prod_nome) . ' - ' . utf8_encode($item->fabricante()->fab_nome) . ' - ' . $item->prod_marca . ' - ' . utf8_encode($item->prod_descricao) ?></option>
            <?php } ?>
        </select>
    </div>

    <label for="for_insc" class="col-sm-1 control-label">Quantidade</label>

    <div class="col-sm-3">
        <input type="text" class="form-control" id="iped_qtd" placeholder="Quantidade">
    </div>

    <div class="col-sm-1">
        <button type="button" id="btnAdd" class="btn btn-primary btn-lg btn-block form-control" style="padding: 0px">
            add
        </button>
    </div>
</div>

<div class="form-group">
    <label for="ent_total" class="col-sm-1 control-label">Total</label>

    <div class="col-sm-3">
        <input type="text" class="form-control" id="ent_total" name="ent_total" placeholder="Total"
               value="<?= isset($data) ? $data->ent_total != null ? $data->ent_total : '' : ''  ?>">
    </div>

</div>

<div class="form-group">
    <div class="col-sm-11">
        <h3>Pedidos Pendente</h3>

        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Usuário</th>
                    <th>Data Criação</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($pedidos as $item) { ?>
                    <tr id="<?= $item->ped_id ?>">
                        <th scope="row"><?= $item->ped_id ?></th>
                        <td><?= $item->usu_nome ?></td>

                        <td><?= date("d/m/Y H:i", strtotime($item->created_at)) ?></td>
                        <td>
                            <button type="button" class="btn btn-primary glyphicon glyphicon-search btnModal"
                                    data-pedido="<?= $item->ped_id ?>" data-toggle="modal"
                                    data-target=".bs-example-modal-lg">

                            </button>
                        </td>
                    </tr>

                <?php } ?>
                </tbody>

            </table>
        </div>

    </div>
</div>

<div class="form-group">
    <div class="col-sm-11">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Produto</th>
                    <th>Fabricante</th>
                    <th>Marca</th>
                    <th>Quantidade</th>
                    <th>Valor unitário</th>
                    <th>Imposto</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody id="conteudoItem">

                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Large modal -->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Produto Pedido</h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
