<div class="form-group">
    <label for="pcat_nome" class="col-sm-1 control-label">Categoria</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="pcat_nome" name="pcat_nome" placeholder="Categoria" required
               value="<?= isset($data) ? $data->pcat_nome != null ? $data->pcat_nome : '' : '' ?>">
    </div>
</div>
