<input type="hidden" name="usu_id" value="<?= $_SESSION['usuario']->usu_id ?>">

<div class="form-group">

    <label for="cid_id" class="col-sm-1 control-label">Produto</label>

    <div class="col-sm-5">
        <select class="form-control" id="prod_id">
            <option value="" selected>Selecione...</option>
            <?php
            foreach ($produtos as $item) {
                $arrProd = json_encode(array(
                    'prod_id' => $item->prod_id,
                    'prod_nome' => utf8_encode($item->prod_nome),
                    'fabricante' => utf8_encode($item->fabricante()->fab_nome),
                    'prod_marca' => utf8_encode($item->prod_marca)
                ))
                ?>
                <option
                    value="<?= $item->prod_id ?>"
                    data-produto='<?= $arrProd ?>'><?= $item->prod_id . ' - ' . utf8_encode($item->prod_nome) . ' - ' . utf8_encode($item->fabricante()->fab_nome) . ' - ' . $item->prod_marca  ?></option>
            <?php } ?>
        </select>
    </div>

    <label for="for_insc" class="col-sm-1 control-label">Quantidade</label>

    <div class="col-sm-3">
        <input type="text" class="form-control" id="iped_qtd" placeholder="Quantidade">
    </div>

    <div class="col-sm-1">
        <button type="button" id="btnAdd" class="btn btn-primary btn-lg btn-block form-control" style="padding: 0px">
            add
        </button>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-offset-1 col-sm-10">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Produto</th>
                    <th>Fabricante</th>
                    <th>Marca</th>
                    <th>Quantidade</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody id="conteudoItem">
                <?php
                if (isset($data))
                    foreach ($data->itemPedido() as $item) {
                        $produto = $item->produto();
                        ?>
                        <tr>
                            <th scope="row"> <?= $item->prod_id ?></th>
                            <td> <?= $produto->prod_nome ?></td>
                            <td> <?= $produto->fabricante()->fab_nome ?></td>
                            <td> <?= $produto->prod_nome ?></td>
                            <td> <?= $item->iped_qtd ?></td>
                            <td>
                                <button type="button" onclick="excluir(this)"
                                        class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                    <?php }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>