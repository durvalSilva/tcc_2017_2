<div class="form-group">
    <label for="trans_id" class="col-sm-1 control-label">Transportadora</label>

    <div class="col-sm-10">
        <select class="form-control" id="trans_id" name="trans_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($transportadoras as $item) {
                    if ($data->trans_id == $item->trans_id) {
                        ?>
                        <option value="<?= $item->trans_id ?>"
                                selected><?= $item->trans_nome ?></option>
                    <?php } else { ?>
                        <option value="<?= $item->trans_id ?>"><?= $item->trans_nome ?></option>
                    <?php }
                }
            } else {
                foreach ($transportadoras as $item) { ?>
                    <option value="<?= $item->trans_id ?>"><?= $item->trans_nome ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="loj_id" class="col-sm-1 control-label">Loja</label>

    <div class="col-sm-10">
        <select class="form-control" id="loj_id" name="loj_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($lojas as $item) {
                    if ($data->loj_id == $item->loj_id) {
                        ?>
                        <option value="<?= $item->loj_id ?>"
                                selected><?= $item->loj_nome ?></option>
                    <?php } else { ?>
                        <option value="<?= $item->loj_id ?>"><?= $item->loj_nome ?></option>
                    <?php }
                }
            } else {
                foreach ($lojas as $item) { ?>
                    <option value="<?= $item->loj_id ?>"><?= $item->loj_nome ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="ent_frete" class="col-sm-1 control-label">Valor do Frete</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="sai_frete" name="sai_frete" placeholder="Frete" required
               value="<?= isset($data) ? $data->sai_frete != null ? $data->sai_frete : '' : '' ?>">
    </div>
</div>

<div class="form-group">
    <label for="ent_imposto" class="col-sm-1 control-label">Valor do Imposto</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="sai_imposto" name="sai_imposto" placeholder="Imposto" required
               value="<?= isset($data) ? $data->sai_imposto != null ? $data->sai_imposto : '' : '' ?>">
    </div>
</div>


<div class="form-group">

    <label for="cid_id" class="col-sm-1 control-label">Produto</label>

    <div class="col-sm-5">
        <select class="form-control" id="prod_id">
            <option value="" selected>Selecione...</option>
            <?php
            foreach ($produtos as $item) {
                $arrProd = json_encode(array(
                    'prod_id' => $item->prod_id,
                    'prod_nome' => utf8_encode($item->prod_nome),
                    'fabricante' => utf8_encode($item->fab_nome),
                    'prod_marca' => utf8_encode($item->prod_marca)
                ))
                ?>
                <option
                        value="<?= $item->prod_id ?>"
                        data-produto='<?= $arrProd ?>'><?= $item->prod_id . ' - ' . utf8_encode($item->prod_nome) . ' - ' . utf8_encode($item->fab_nome) . ' - ' . $item->prod_marca ?></option>
            <?php } ?>
        </select>
    </div>

    <label for="for_insc" class="col-sm-1 control-label">Quantidade</label>

    <div class="col-sm-3">
        <input type="text" class="form-control" id="iped_qtd" placeholder="Quantidade">
    </div>

    <div class="col-sm-1">
        <button type="button" id="btnAdd" class="btn btn-primary btn-lg btn-block form-control" style="padding: 0px">
            add
        </button>
    </div>
</div>

<div class="form-group">
    <label for="ent_total" class="col-sm-1 control-label">Total</label>

    <div class="col-sm-3">
        <input type="text" class="form-control" id="sai_total" name="sai_total" placeholder="Total"
               value="<?= isset($data) ? $data->sai_total != null ? $data->sai_total : '' : '' ?>">
    </div>

</div>

<div class="form-group">
    <div class="col-sm-11">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Produto</th>
                    <th>Fabricante</th>
                    <th>Marca</th>
                    <th>Quantidade</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody id="conteudoItem">

                </tbody>
            </table>
        </div>
    </div>
</div>


