<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header">
        <h1>Entrada</h1>
    </div>
    <div class="row">
        <div class="row">
            <div style="display: inline-grid;font-size: 2rem;">
                <span style="margin-bottom: 10px;"><strong>Usuario:</strong> <?= $data->usuario()->usu_nome ?></span>
                <span style="margin-bottom: 10px;"><strong>Transportadora:</strong> <?= $data->transportadora()->trans_nome ?></span>
                <span style="margin-bottom: 10px;"><strong>Loja:</strong> <?= $data->loja()->loj_nome ?></span>
                <span style="margin-bottom: 10px;"><strong>Loja:</strong> <?= $data->loja()->loj_nome ?></span>
                <span style="margin-bottom: 10px;"><strong>Total:</strong> <?= $data->sai_total ?></span>
                <span style="margin-bottom: 10px;"><strong>Frete:</strong> <?= $data->sai_frete ?></span>
                <span style="margin-bottom: 10px;"><strong>Imposto:</strong> <?= $data->sai_imposto ?></span>
            </div>
        </div>
        <div class="row">
            <h2>Itens Saida</h2>

            <?php
            foreach ($data->ItemSaida() as $item) {
                ?>
                <div class="col-sm-3" style="display: inline-grid;font-size: 2rem;">
                    <span style="margin-bottom: 10px;"><strong>Produto:</strong> <?= $item->produto()->prod_nome ?></span>
                    <span style="margin-bottom: 10px;"><strong>Quantidade:</strong> <?= $item->isai_qtd ?></span>
                    <span style="margin-bottom: 10px;"><strong>Valor:</strong> <?= $item->isai_valor ?></span>
                    <span style="margin-bottom: 10px;"><strong>Imposto:</strong> <?= $item->isai_imposto ?></span>
                </div>
            <?php }

            ?>
        </div>
    </div>

</div>

<?php require_once('public/view/admin/template/footer.php') ?>

</body>
</html>
