<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header" style="    display: inline-flex;width: 100%;">
        <div class="col-lg-12 col-md-12">
            <div class="col-lg-3 col-md-5">
                <h1>Saida de produto</h1>

            </div>
            <div class="col-lg-1 col-md-2 col-md-offset-5 col-lg-offset-9">
                <a type="button" class="btn btn-success btn-lg btn-block" href="saidaProduto/create">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-top: 20%;"></span> Add
                </a>
            </div>
        </div>
    </div>
    <?php require_once('public/view/admin/template/mensagem.php') ?>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Transportadora</th>
                <th>V.Total</th>
                <th>Frete</th>
                <th>Loja</th>
                <th>Data Saida</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $item) { ?>
                <tr>
                    <th scope="row"><?= $item->sai_id ?></th>
                    <td><?= utf8_encode($item->transportadora()->trans_nome)  ?></td>
                    <td><?= $item->sai_total ?></td>
                    <td><?= $item->sai_frete ?></td>
                    <td><?= utf8_encode($item->loja()->loj_nome) ?></td>
                    <td><?= date("d/m/Y H:i", strtotime($item->created_at)) ?></td>
                    <td>

                        <a href="<?= url_base('saidaProduto/detalhes/' . $item->sai_id) ?>" type="button"
                           class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                        <?php

                        if($_SESSION['usuario']->tipo->tusu_id == 1){?>
                            <a href="<?= url_base('saidaProduto/delete/' . $item->sai_id) ?>" type="button"
                               class="btn btn-default" aria-label="Left Align">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </a>

                        <?php }
                        ?>
                    </td>
                </tr>

            <?php } ?>
            </tbody>

        </table>
    </div>
</div>

<?php require_once('public/view/admin/template/footer.php') ?>
</body>
</html>
