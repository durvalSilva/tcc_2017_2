<!DOCTYPE html>
<html lang="pt-br">

<?php require_once('public/view/admin/template/head.php'); ?>
<body>
<?php require_once('public/view/admin/template/topMenu.php') ?>
<?php require_once('public/view/admin/template/sideMenu.php') ?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="page-header" style="    display: inline-flex;width: 100%;">
        <div class="col-lg-12 col-md-12">
            <div class="col-lg-2 col-md-1">
                <h1>Produtos</h1>

            </div>
            <div class="col-lg-1 col-md-2 col-md-offset-9 col-lg-offset-9">
                <a type="button" class="btn btn-success btn-lg btn-block" href="produto/create">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-top: 20%;"></span> Add
                </a>
            </div>
        </div>
    </div>
    <?php require_once('public/view/admin/template/mensagem.php') ?>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Marca</th>
                <th>Qtd.Minima</th>
                <th>Categoria</th>
                <th>Fabricante</th>
                <th>Data Criação</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $item) { ?>
                <tr>
                    <th scope="row"><?= $item->prod_id ?></th>
                    <td><?= $item->prod_nome ?></td>
                    <td><?= utf8_encode($item->prod_descricao) ?></td>
                    <td><?= $item->prod_marca ?></td>
                    <td><?= $item->prod_qtd_min ?></td>
                    <td><?= $item->categoria()->pcat_nome ?></td>
                    <td><?= $item->fabricante()->fab_nome ?></td>
                    <td><?= date("d/m/Y H:i", strtotime($item->created_at)) ?></td>
                    <td>
                        <a href="<?= url_base('produto/edit/' . $item->prod_id) ?>" type="button"
                           class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        <a href="<?= url_base('produto/delete/' . $item->prod_id) ?>" type="button"
                           class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>

            <?php } ?>
            </tbody>

        </table>
    </div>
</div>

<?php require_once('public/view/admin/template/footer.php') ?>
</body>
</html>
