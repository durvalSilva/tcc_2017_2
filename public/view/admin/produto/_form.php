<div class="form-group">
    <label for="fab_id" class="col-sm-1 control-label">Fabricante</label>

    <div class="col-sm-10">
        <select class="form-control" id="fab_id" name="fab_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($fabricantes as $item) {
                    if ($item->fab_id == $data->fab_id) {
                        ?>
                        <option value="<?= $item->fab_id ?>"
                                selected><?= $item->fab_nome ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->fab_id ?>"><?= $item->fab_nome ?></option>
                    <?php }
                }
            } else {
                foreach ($fabricantes as $item) { ?>
                    <option value="<?= $item->fab_id ?>"><?= $item->fab_nome ?></option>
                <?php }
            } ?>
        </select>
    </div>

</div>


<div class="form-group">
    <label for="prod_nome" class="col-sm-1 control-label">Produto</label>

    <div class="col-sm-4">
        <input type="text" class="form-control" id="prod_nome" name="prod_nome" placeholder="Produto" required
               value="<?= isset($data) ? $data->prod_nome != null ? $data->prod_nome : '' : '' ?>">
    </div>

    <label for="fab_id" class="col-sm-1 control-label">Categoria</label>

    <div class="col-sm-5">
        <select class="form-control" id="pcat_id" name="pcat_id" required>
            <option value="" selected>Selecione...</option>
            <?php
            if (isset($data)) {
                foreach ($categorias as $item) {
                    if ($item->pcat_id == $data->pcat_id) {
                        ?>
                        <option value="<?= $item->pcat_id ?>" selected><?= $item->pcat_nome ?></option>
                    <?php } else { ?>

                        <option value="<?= $item->pcat_id ?>"><?= $item->pcat_nome ?></option>
                    <?php }
                }
            } else {
                foreach ($categorias as $item) { ?>
                    <option value="<?= $item->pcat_id ?>"><?= $item->pcat_nome ?></option>
                <?php }
            } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="prod_marca" class="col-sm-1 control-label">Marca</label>

    <div class="col-sm-4">
        <input type="text" class="form-control" id="prod_marca" name="prod_marca" placeholder="Marca" required
               value="<?= isset($data) ? $data->prod_marca != null ? $data->prod_marca : '' : '' ?>">
    </div>

    <label for="prod_qtd_min" class="col-sm-1 control-label">Qtd.Minima</label>

    <div class="col-sm-5">
        <input type="text" class="form-control" id="prod_qtd_min" name="prod_qtd_min" placeholder="Qtd.Minima" required
               value="<?= isset($data) ? $data->prod_qtd_min != null ? $data->prod_qtd_min : '' : '' ?>">
    </div>
</div>


<div class="form-group">
    <label for="prod_descricao" class="col-sm-1 control-label">Descrição</label>

    <div class="col-sm-10">
        <textarea id="prod_descricao" name="prod_descricao" class="form-control" rows="5"><?= isset($data) ? $data->prod_descricao != null ? utf8_encode($data->prod_descricao) : '' : '' ?></textarea>
    </div>
</div>

