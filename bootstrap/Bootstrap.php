<?php

/**
 * @author: Edval Silva
 * @version 0.1
 * Responsável por Instanciar automaticamente as classes
 */

spl_autoload_register(function ($class) {
    $file = str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require_once($file);
    } else {
//        trigger_error("Arquivo não existe: $file", E_USER_WARNING);
        require_once('app/controller/DashboardController.php');
    }
});






