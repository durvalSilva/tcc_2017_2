<?php

namespace app\model\service;

use app\model\entity\ItemPedido;
use app\model\entity\PedidoCompra;

class PedidoCompraService
{
    private $db;

    public function __construct()
    {
        $this->db = new PedidoCompra();
    }

    public function index()
    {
        return $this->db->orderBy('ped_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
//criar transação

        $this->db->usu_id = $data['usu_id'];
        $this->db->created_at = 'NOW()';
        $this->db->updated_at = 'NOW()';
        $pedido = $this->db->create();

        foreach (json_decode($data['itens_pedido']) as $p) {
            $item = new ItemPedido();
            $item->prod_id = $p->prod_id;
            $item->ped_id = $pedido->ped_id;
            $item->iped_qtd = $p->iped_qtd;
            $item->create();
        }


        return ['status' => 'sucesso', 'mensagem' => 'Pedido Cadastrado com Sucesso !'];
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['ped_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $pedido['usu_id'] = $data['usu_id'];
        $this->db->update($pedido, $id);

        $sql = "DELETE FROM estoque.itens_pedido
                WHERE ped_id = $id";

        $this->db->customQuery($sql);

        foreach (json_decode($data['itens_pedido']) as $p) {
            $item = new ItemPedido();
            $item->prod_id = $p->prod_id;
            $item->ped_id = $id;
            $item->iped_qtd = $p->iped_qtd;
            $item->create();
        }

        return ['status' => 'sucesso', 'mensagem' => 'Pedido atualizado com Sucesso !'];
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Pedido deletado com Sucesso !'];
    }

    public function pedidoJson($id)
    {
        $sql = "SELECT 
                p.ped_id
                , p.created_at
                , u.usu_id
                , u.usu_nome	
              FROM estoque.pedidos p
              , acesso.usuarios u
              WHERE 
                p.usu_id = u.usu_id
                AND p.ped_id = $id
                AND p.deleted_at IS NULL
                AND u.deleted_at IS NULL";
        $data = $this->db->customQuery($sql)[0];

        $sql = "SELECT ip.iped_id
                    , ip.ped_id
                    , ip.iped_qtd
                    , ip.created_at
                    , p.prod_id
                    , p.prod_nome
                    , p.prod_descricao
                    , p.prod_marca
                    , c.pcat_id
                    , c.pcat_nome
                    , f.fab_id
                    , f.fab_nome
                    , f.fab_cnpj
                  FROM estoque.itens_pedido ip
                  , estoque.produtos p
                  , estoque.produto_categoria c
                  ,cadastro.fabricantes f
                WHERE ped_id = $id
                AND ip.prod_id = p.prod_id
                AND p.pcat_id = c.pcat_id
                AND p.fab_id = f.fab_id";
        $result = $this->db->customQuery($sql);
        $arrItens = array();
        foreach ($result as $item) {
            $itens = new \stdClass;
            $itens->iped_id = $item->iped_id;
            $itens->ped_id = $item->ped_id;
            $itens->iped_qtd = $item->iped_qtd;
            $itens->prod_id = $item->prod_id;
            $itens->prod_nome = utf8_encode($item->prod_nome);
            $itens->prod_descricao = utf8_encode($item->prod_descricao);
            $itens->prod_marca = utf8_encode($item->prod_marca);
            $itens->pcat_id = $item->pcat_id;
            $itens->pcat_nome = utf8_encode($item->pcat_nome);
            $itens->fab_id = $item->fab_id;
            $itens->fab_nome = utf8_encode($item->fab_nome);
            $itens->fab_cnpj = $item->fab_cnpj;
            $itens->created_at = $item->created_at;
            array_push($arrItens, $itens);
        }

        $pedido = new \stdClass;
        $pedido->ped_id = $data->ped_id;
        $pedido->usu_id = $data->usu_id;
        $pedido->usu_nome = $data->usu_nome;
        $pedido->itensPedido = $arrItens;
        return $pedido;
    }

}