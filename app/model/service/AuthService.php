<?php

namespace app\model\service;

use app\model\entity\Usuario;

class AuthService
{
    private $db;

    public function __construct()
    {
        $this->db = new Usuario();
    }

    private function _encryptSenha($senha)
    {
        return hash('whirlpool', $senha);
    }

    private function usuarioExiste()
    {
        return $this->db->findWhere(array(
            ['usu_email', $this->db->usu_email],
            ['usu_senha', $this->db->usu_senha],
            ['deleted_at', 'IS', 'NULL']
        ))->fetch();
    }

    public function logar(array $data)
    {
        $this->db->usu_email = $data['usu_email'];
        $this->db->usu_senha = $this->_encryptSenha($data['usu_senha']);


        $result = $this->usuarioExiste();

        if (!empty($result)) {
            $tipo = $result->TipoUsuario();

            $tipoUsuario = new \stdClass;
            $tipoUsuario->tusu_id = $tipo->tusu_id;
            $tipoUsuario->tusu_nome = $tipo->tusu_nome;

            $usuario = new \stdClass;
            $usuario->usu_id = $result->usu_id;
            $usuario->usu_email = $result->usu_email;
            $usuario->usu_senha = $result->usu_senha;
            $usuario->tipo = $tipoUsuario;
            $_SESSION['usuario'] = $usuario;
            return true;
        }
        session_destroy();
        header('Location: '.url_base());
    }

    public function autorizar()
    {
        if (array_key_exists('usuario',$_SESSION)) {
            $this->db->usu_email = $_SESSION['usuario']->usu_email;
            $this->db->usu_senha = $_SESSION['usuario']->usu_senha;
            if (!empty($this->usuarioExiste()))
                return true;
        }
        header('Location: '.url_base());
    }


}