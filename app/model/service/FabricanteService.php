<?php

namespace app\model\service;

use app\model\entity\Fabricante;
use app\model\validator\FabricanteValidator;

class FabricanteService
{
    private $db;

    public function __construct()
    {
        $this->db = new Fabricante();
    }

    public function index()
    {
        return $this->db->orderBy('fab_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        $validacao = new FabricanteValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $this->db->fab_nome = $data['fab_nome'];
            $this->db->fab_logradouro = $data['fab_logradouro'];
            $this->db->fab_numero = $data['fab_numero'];
            $this->db->fab_bairro = $data['fab_bairro'];
            $this->db->fab_cep = $data['fab_cep'];
            $this->db->fab_cnpj = $data['fab_cnpj'];
            $this->db->cid_id = $data['cid_id'];
            $this->db->created_at = 'NOW()';
            $this->db->updated_at = 'NOW()';
            $fabricante = $this->db->create();
            return ['status' => 'sucesso', 'mensagem' => 'Fabricante Cadastrado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['fab_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $validacao = new FabricanteValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $this->db->update($data, $id);
            return ['status' => 'sucesso', 'mensagem' => 'Fabricante atualizado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Fabricante deletado com Sucesso !'];
    }

}