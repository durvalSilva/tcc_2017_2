<?php

namespace app\model\service;

use app\model\entity\Fornecedor;
use app\model\validator\FornecedorValidator;

class FornecedorService
{
    private $db;

    public function __construct()
    {
        $this->db = new Fornecedor();
    }

    public function index()
    {
        return $this->db->orderBy('for_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        try {
            $validacao = new FornecedorValidator();
            $validacao = $validacao->validacao($data);
            if ($validacao === true) {
                $this->db->for_nome = $data['for_nome'];
                $this->db->for_logradouro = $data['for_logradouro'];
                $this->db->for_rsocial = $data['for_rsocial'];
                $this->db->for_insc = $data['for_insc'];
                $this->db->for_numero = $data['for_numero'];
                $this->db->for_bairro = $data['for_bairro'];
                $this->db->for_cep = $data['for_cep'];
                $this->db->for_cnpj = $data['for_cnpj'];
                $this->db->cid_id = $data['cid_id'];
                $this->db->created_at = 'NOW()';
                $this->db->updated_at = 'NOW()';
                $fornecedor = $this->db->create();
                return ['status' => 'sucesso', 'mensagem' => 'Fornecedor Cadastrado com Sucesso !'];
            } else {
                return ['status' => 'erro', 'mensagem' => $validacao];
            }
        } catch (\PDOException $e) {
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de cadastrar o Fornecedor !'];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['for_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        try {
            $validacao = new FornecedorValidator();
            $validacao = $validacao->validacao($data);
            if ($validacao === true) {
                $this->db->update($data, $id);
                return ['status' => 'sucesso', 'mensagem' => 'Fornecedor atualizado com Sucesso !'];
            } else {
                return ['status' => 'erro', 'mensagem' => $validacao];
            }
        } catch (\PDOException $e) {
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de atualizar o Fornecedor !'];
        }
    }

    public function delete($id)
    {
        try {
            $this->db->delete($id);
            return ['status' => 'sucesso', 'mensagem' => 'Fornecedor deletado com Sucesso !'];
        } catch (\PDOException $e) {
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de excluir o Fornecedor !'];
        }
    }

}