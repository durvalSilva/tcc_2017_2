<?php

namespace app\model\service;

use app\model\entity\ProdutoCategoria;

class ProdutoCategoriaService
{
    private $db;

    public function __construct()
    {
        $this->db = new ProdutoCategoria();
    }

    public function index()
    {
        return $this->db->orderBy('pcat_id','ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {

        $this->db->pcat_nome = $data['pcat_nome'];
        $this->db->created_at = 'NOW()';
        $this->db->updated_at = 'NOW()';
        $categoria = $this->db->create();
        return ['status' => 'sucesso', 'mensagem' => 'categoria Cadastrado com Sucesso !'];
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['pcat_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $this->db->update($data, $id);
        return ['status' => 'sucesso', 'mensagem' => 'Categoria atualizada com Sucesso !'];
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Categoria deletado com Sucesso !'];
    }


}