<?php

namespace app\model\service;

use app\model\entity\Transportadora;
use app\model\validator\TransportadoraValidator;

class TransportadoraService
{
    private $db;

    public function __construct()
    {
        $this->db = new Transportadora();
    }

    public function index()
    {
        return $this->db->orderBy('trans_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        try {
            $validacao = new TransportadoraValidator();
            $validacao = $validacao->validacao($data);
            if ($validacao === true) {
                $this->db->trans_nome = $data['trans_nome'];
                $this->db->trans_logradouro = $data['trans_logradouro'];
                $this->db->trans_numero = $data['trans_numero'];
                $this->db->trans_bairro = $data['trans_bairro'];
                $this->db->trans_cep = $data['trans_cep'];
                $this->db->trans_cnpj = $data['trans_cnpj'];
                $this->db->cid_id = $data['cid_id'];
                $this->db->created_at = 'NOW()';
                $this->db->updated_at = 'NOW()';
                $transportadoras = $this->db->create();
                return ['status' => 'sucesso', 'mensagem' => 'Transportadora Cadastrada com Sucesso !'];
            } else {
                return ['status' => 'erro', 'mensagem' => $validacao];
            }
        } catch (\PDOException $e) {
            print_r($e->getMessage());die;
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de cadastrar a Transportadora !'];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['trans_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        try {
            $validacao = new TransportadoraValidator();
            $validacao = $validacao->validacao($data);
            if ($validacao === true) {
                $this->db->update($data, $id);
                return ['status' => 'sucesso', 'mensagem' => 'Transportadora atualizada com Sucesso !'];
            } else {
                return ['status' => 'erro', 'mensagem' => $validacao];
            }
        } catch (\PDOException $e) {
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de atualizar a Transportadora !'];
        }
    }

    public function delete($id)
    {
        try {
            $this->db->delete($id);
            return ['status' => 'sucesso', 'mensagem' => 'Transportadora deletado com Sucesso !'];
        } catch (\PDOException $e) {
            return ['status' => 'erro', 'mensagem' => 'Erro na tentativa de excluir a Transportadora !'];
        }
    }

}