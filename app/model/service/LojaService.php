<?php

namespace app\model\service;

use app\model\entity\Loja;
use app\model\validator\LojaValidator;


class LojaService
{
    private $db;

    public function __construct()
    {
        $this->db = new Loja();
    }

    public function index()
    {
        return $this->db->orderBy('loj_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {

        $validacao = new LojaValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $this->db->loj_nome = $data['loj_nome'];
            $this->db->loj_logradouro = $data['loj_logradouro'];
            $this->db->loj_numero = $data['loj_numero'];
            $this->db->loj_bairro = $data['loj_bairro'];
            $this->db->loj_cep = $data['loj_cep'];
            $this->db->loj_cnpj = $data['loj_cnpj'];
            $this->db->loj_insc = $data['loj_insc'];
            $this->db->cid_id = $data['cid_id'];
            $this->db->created_at = 'NOW()';
            $this->db->updated_at = 'NOW()';
            $loja = $this->db->create();
            return ['status' => 'sucesso', 'mensagem' => 'Loja Cadastrado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['loj_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $validacao = new LojaValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $this->db->update($data, $id);
            return ['status' => 'sucesso', 'mensagem' => 'Loja atualizado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Loja deletado com Sucesso !'];
    }

}