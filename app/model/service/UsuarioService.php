<?php

namespace app\model\service;

use app\model\entity\Usuario;
use app\model\validator\UsuarioValidator;

class UsuarioService
{
    private $db;

    public function __construct()
    {
        $this->db = new Usuario();
    }

    public function index()
    {
        return $this->db->orderBy('usu_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        $validacao = new UsuarioValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $this->db->usu_nome = utf8_decode($data['usu_nome']);
            $this->db->usu_email = utf8_decode($data['usu_email']);
            $this->db->usu_senha = utf8_decode(hash('whirlpool', $data['usu_senha']));
            $this->db->tusu_id = $data['tusu_id'];
            $this->db->created_at = 'NOW()';
            $this->db->updated_at = 'NOW()';
            $usuario = $this->db->create();
            return ['status' => 'sucesso', 'mensagem' => 'Usuario Cadastrado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['usu_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $validacao = new UsuarioValidator();
        $validacao = $validacao->validacaoUpdate($data);
        if ($validacao === true) {
            if ($data['usu_senha'] == '******') {
                unset($data['usu_senha']);
            }
            $this->db->update($data, $id);
            return ['status' => 'sucesso', 'mensagem' => 'Usúario atualizado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Usúario deletado com Sucesso !'];
    }

}