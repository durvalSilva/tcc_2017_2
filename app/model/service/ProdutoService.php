<?php

namespace app\model\service;

use app\model\entity\Produto;
use app\model\validator\ProdutoValidator;

class ProdutoService
{
    private $db;

    public function __construct()
    {
        $this->db = new Produto();
    }

    public function index()
    {
        return $this->db->orderBy('prod_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        $validacao = new ProdutoValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {

            $this->db->prod_nome = $data['prod_nome'];
            $this->db->prod_marca = $data['prod_marca'];
            $this->db->prod_descricao = utf8_decode($data['prod_descricao']);
            $this->db->prod_qtd_min = $data['prod_qtd_min'];
            $this->db->fab_id = $data['fab_id'];
            $this->db->for_id = $data['for_id'];
            $this->db->pcat_id = $data['pcat_id'];
            $this->db->created_at = 'NOW()';
            $this->db->updated_at = 'NOW()';
            $produto = $this->db->create();

            return ['status' => 'sucesso', 'mensagem' => 'Produto Cadastrado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }

    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['prod_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $validacao = new ProdutoValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            $data['prod_descricao'] = utf8_decode($data['prod_descricao']);
            $this->db->update($data, $id);
            return ['status' => 'sucesso', 'mensagem' => 'Produto atualizado com Sucesso !'];
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Produto deletado com Sucesso !'];
    }

    public function getProdutosInEstoque()
    {
        $sql = "SELECT DISTINCT p.prod_id
                , p.prod_nome
                , p.prod_descricao
                , p.prod_marca
                , p.prod_qtd_min
                , f.fab_nome
                , pc.pcat_nome
                , (SELECT COALESCE((SELECT SUM(ient_qtd)
                        FROM estoque.itens_entrada
                        WHERE deleted_at IS NULL
                        AND prod_id = p.prod_id),0) - COALESCE((SELECT SUM (isai_qtd)
                        FROM estoque.itens_saida
                        WHERE deleted_at IS NULL
                        AND prod_id = p.prod_id),0)) as qtd_estoque
              FROM estoque.produtos p
              , estoque.itens_entrada ie
              , cadastro.fabricantes f
              , estoque.produto_categoria pc
              WHERE p.prod_id IN (SELECT prod_id
                        FROM estoque.itens_entrada
                        WHERE deleted_at IS NULL)
              AND p.prod_id = ie.prod_id
              AND p.fab_id = f.fab_id
              AND p.pcat_id = pc.pcat_id";
        return $this->db->customQuery($sql);
    }
}