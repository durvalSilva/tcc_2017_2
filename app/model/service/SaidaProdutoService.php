<?php

namespace app\model\service;

use app\model\entity\ItemSaidaProduto;
use app\model\entity\SaidaProduto;
use app\model\validator\SaidaprodutoValidator;

class SaidaProdutoService
{
    private $db;

    public function __construct()
    {
        $this->db = new SaidaProduto();
    }

    public function index()
    {
        return $this->db->orderBy('sai_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        $validacao = new SaidaprodutoValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            try {
                $this->db->beginTransaction();

                $this->db->trans_id = $data['trans_id'];
                $this->db->loj_id = $data['loj_id'];
                $this->db->usu_id = $_SESSION['usuario']->usu_id;
                $this->db->sai_total = str_replace(array('.', ','), array('', '.'), $data['sai_total']);;
                $this->db->sai_frete = str_replace(array('.', ','), array('', '.'), $data['sai_frete']);
                $this->db->sai_imposto = str_replace(array('.', ','), array('', '.'), $data['sai_imposto']);
                $this->db->created_at = 'NOW()';
                $this->db->updated_at = 'NOW()';
                $saida = $this->db->create();

                foreach (json_decode($data['itens_saida']) as $p) {
                    $item = new ItemSaidaProduto();
                    $item->sai_id = $saida->sai_id;
                    $item->prod_id = $p->prod_id;
                    $item->isai_qtd = $p->isai_qtd;
                    $item->isai_valor = str_replace(array('.', ','), array('', '.'), $p->isai_valor);
                    $item->isai_imposto = str_replace(array('.', ','), array('', '.'), $p->isai_imposto);
                    $item->create();
                }

                $this->db->commit();
                return ['status' => 'sucesso', 'mensagem' => 'Saida Cadastrado com Sucesso !'];
            } catch (\PDOException $e) {
                $this->db->rollback();
                return ['status' => 'erro', 'mensagem' => 'Ops...Aconteceu um erro'];
            } catch (\Exception $e) {
                $this->db->rollback();
                return ['status' => 'erro', 'mensagem' => 'Ops...Aconteceu um erro'];
            }
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['sai_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $this->db->update($data, $id);
        return ['status' => 'sucesso', 'mensagem' => 'Saida atualizado com Sucesso !'];
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Saida deletado com Sucesso !'];
    }

}