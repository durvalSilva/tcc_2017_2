<?php

namespace app\model\service;

use app\model\entity\EntradaProduto;
use app\model\entity\ItemEntradaProduto;
use app\model\validator\EntradaprodutoValidator;

class EntradaProdutoService
{
    private $db;

    public function __construct()
    {
        $this->db = new EntradaProduto();
    }

    public function index()
    {
        return $this->db->orderBy('ent_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        $validacao = new EntradaprodutoValidator();
        $validacao = $validacao->validacao($data);
        if ($validacao === true) {
            try {
                $this->db->beginTransaction();
                $this->db->trans_id = $data['trans_id'];
                $this->db->ent_num_nf = $data['ent_num_nf'];
                $this->db->usu_id = $_SESSION['usuario']->usu_id;
                $this->db->ent_frete = str_replace(array('.', ','), array('', '.'), $data['ent_frete']);
                $this->db->ent_imposto = str_replace(array('.', ','), array('', '.'), $data['ent_imposto']);
                $this->db->ent_total = str_replace(array('.', ','), array('', '.'), $data['ent_total']);
                $this->db->created_at = 'NOW()';
                $this->db->updated_at = 'NOW()';
                $entrada = $this->db->create();
                $ped_anterior = '';
                foreach (json_decode($data['itens_entrada']) as $p) {
                    $ped_id = $p->ped_id;
                    if (!empty($ped_id) && $ped_id != $ped_anterior) {
                        $ped_anterior = $ped_id;
                        $sql = "INSERT INTO estoque.entrada_pedido(ped_id, ent_id) VALUES ($ped_anterior, {$entrada->ent_id})";
                        $this->db->customQuery($sql, false);
                    }

                    $item = new ItemEntradaProduto();
                    $item->ent_id = $entrada->ent_id;
                    $item->prod_id = $p->prod_id;
                    $item->ient_qtd = $p->ient_qtd;
                    $item->ient_valor = str_replace(array('.', ','), array('', '.'), $p->ient_valor);
                    $item->ient_imposto = str_replace(array('.', ','), array('', '.'), $p->ient_imposto);
                    $item->create();
                }
                $this->db->commit();
                return ['status' => 'sucesso', 'mensagem' => 'Entrada Cadastrado com Sucesso !'];
            } catch (\PDOException $e) {
                $this->db->rollback();
                return ['status' => 'erro', 'mensagem' => 'Ops...Aconteceu um erro'];
            } catch (\Exception $e) {
                $this->db->rollback();
                return ['status' => 'erro', 'mensagem' => 'Ops...Aconteceu um erro'];
            }
        } else {
            return ['status' => 'erro', 'mensagem' => $validacao];
        }
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['ent_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $this->db->update($data, $id);
        return ['status' => 'sucesso', 'mensagem' => 'Entrada atualizado com Sucesso !'];
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Entrada deletada com Sucesso !'];
    }

    public function getPedidos()
    {
        $sql = "SELECT ep.ped_id
                    , ep.created_at
                    , ep.updated_at
                    , ep.deleted_at
                    , u.usu_nome
                 FROM estoque.pedidos ep
                    ,acesso.usuarios u
                 WHERE ep.deleted_at IS NULL
                 AND u.deleted_at IS NULL
                 AND ped_id NOT IN (SELECT ped_id
                                    FROM estoque.entrada_pedido)
                 AND ep.usu_id = u.usu_id";
        return $this->db->customQuery($sql);
    }

}