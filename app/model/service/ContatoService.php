<?php

namespace app\model\service;

use app\model\entity\Contato;

class ContatoService
{
    private $db;

    public function __construct()
    {
        $this->db = new Contato();
    }

    public function index()
    {
        return $this->db->orderBy('con_id', 'ASC')->all()->fetchAll();
    }

    public function store(array $data)
    {
        if (array_key_exists('fab_id', $data)) {
            $this->db->fab_id = $data['fab_id'];
            $redirect = 'fabricante/'.$data['fab_id'];
        } else {
            $this->db->for_id = $data['for_id'];
            $redirect = 'fornecedor/'.$data['for_id'];
        }
        $this->db->con_nome = $data['con_nome'];
        $this->db->con_cargo = $data['con_cargo'];
        $this->db->con_fone = $data['con_fone'];
        $this->db->created_at = 'NOW()';
        $this->db->updated_at = 'NOW()';
        $Contato = $this->db->create();

        return ['status' => 'sucesso', 'mensagem' => 'Contato Cadastrado com Sucesso !','redirect'=>$redirect];
    }

    public function edit($id)
    {
        return $this->db->findWhere(array(['con_id', $id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function update($id, array $data)
    {
        $this->db->update($data, $id);
        return ['status' => 'sucesso', 'mensagem' => 'Contato atualizado com Sucesso !'];
    }

    public function delete($id)
    {
        $this->db->delete($id);
        return ['status' => 'sucesso', 'mensagem' => 'Contato deletado com Sucesso !'];
    }

}