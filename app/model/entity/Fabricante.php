<?php

namespace app\model\entity;

use system\Model;

class Fabricante extends Model
{

    protected $table = 'cadastro.fabricantes';
    protected $primaryKey = 'fab_id';
    protected $fillable = ['fab_nome', 'fab_cnpj', 'fab_logradouro', 'fab_numero', 'fab_cep', 'fab_bairro', 'cid_id', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.fabricantes_fab_id_seq';
    private $fab_id;
    private $fab_nome;
    private $fab_cnpj;
    private $fab_logradouro;
    private $fab_numero;
    private $fab_cep;
    private $fab_bairro;
    private $created_at;
    private $updated_at;
    private $deleted_at;
    private $cid_id;


    public function cidade()
    {
        $cidade = new Cidade();
        return $cidade->findWhere(array(['cid_id', $this->cid_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

