<?php

namespace app\model\entity;

use system\Model;

class ProdutoCategoria extends Model
{

    protected $table = 'estoque.produto_categoria';
    protected $primaryKey = 'pcat_id';
    protected $fillable = ['pcat_nome', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.produto_categoria_pcat_id_seq';
    private $pcat_id;
    private $pcat_nome;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }


}

