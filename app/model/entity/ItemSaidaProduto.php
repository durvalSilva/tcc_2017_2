<?php

namespace app\model\entity;

use system\Model;

class ItemSaidaProduto extends Model
{

    protected $table = 'estoque.itens_saida';
    protected $primaryKey = 'isai_id';
    protected $fillable = ['prod_id', 'sai_id', 'isai_qtd', 'isai_valor', 'isai_imposto', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.itens_saida_isai_id_seq';
    private $isai_id;
    private $prod_id;
    private $sai_id;
    private $isai_qtd;
    private $isai_valor;
    private $isai_imposto;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function produto()
    {
        $item = new Produto();
        return $item->findWhere(array(['prod_id', $this->prod_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function saida()
    {
        $item = new SaidaProduto();
        return $item->findWhere(array(['sai_id', $this->sai_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

