<?php

namespace app\model\entity;

use system\Model;

class Transportadora extends Model
{

    protected $table = 'cadastro.transportadoras';
    protected $primaryKey = 'trans_id';
    protected $fillable = ['trans_nome', 'trans_logradouro', 'trans_numero', 'trans_cep', 'trans_bairro', 'trans_cnpj', 'cid_id', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.transportadoras_trans_id_seq';
    private $trans_id;
    private $trans_nome;
    private $trans_cnpj;
    private $trans_logradouro;
    private $trans_numero;
    private $trans_cep;
    private $trans_bairro;
    private $created_at;
    private $updated_at;
    private $deleted_at;
    private $cid_id;


    public function cidade(){
        $cidade = new Cidade();
        return $cidade->findWhere(array(['cid_id', $this->cid_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
     return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }


}