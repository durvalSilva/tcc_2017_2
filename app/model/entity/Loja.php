<?php

namespace app\model\entity;

use system\Model;

class Loja extends Model
{

    protected $table = 'cadastro.lojas';
    protected $primaryKey = 'loj_id';
    protected $fillable = ['loj_nome', 'loj_cnpj', 'loj_logradouro', 'loj_numero', 'loj_cep', 'loj_bairro', 'loj_insc', 'cid_id', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.lojas_loj_id_seq';
    private $loj_id;
    private $loj_nome;
    private $loj_cnpj;
    private $loj_logradouro;
    private $loj_numero;
    private $loj_cep;
    private $loj_bairro;
    private $loj_insc;
    private $created_at;
    private $updated_at;
    private $deleted_at;
    private $cid_id;

    public function cidade()
    {
        $cidade = new Cidade();
        return $cidade->findWhere(array(['cid_id', $this->cid_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __toString()
    {
        $object = array();
        foreach ($this as $key => $value) {
            $object[$key] = $value;
        }
        return json_encode($object);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

