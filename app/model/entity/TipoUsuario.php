<?php

namespace app\model\entity;

use system\Model;

class TipoUsuario extends Model
{

    protected $table = 'acesso.tipo_usuario';
    protected $primaryKey = 'tusu_id';
    protected $fillable = ['tusu_nome', 'created_at', 'updated_at'];
    protected $sequence = 'acesso.tipo_usuario_tusu_id_seq';
    private $tusu_id;
    private $tusu_nome;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }

}