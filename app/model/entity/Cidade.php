<?php

namespace app\model\entity;

use system\Model;

class Cidade extends Model
{
    protected $table = 'cadastro.cidades';
    protected $primaryKey = 'cid_id';
    private $cid_id;
    private $cid_nome;
    private $uf_id;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function uf()
    {
        $uf = new uf();
        return $uf->findWhere(array(['uf_id', $this->uf_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}
