<?php

namespace app\model\entity;

use system\Model;

class Fornecedor extends Model
{

    protected $table = 'cadastro.fornecedores';
    protected $primaryKey = 'for_id';
    protected $fillable = ['for_nome', 'for_cnpj', 'for_rsocial', 'for_insc', 'for_logradouro', 'for_numero', 'for_cep', 'for_bairro', 'cid_id', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.fornecedores_for_id_seq';
    private $for_id;
    private $for_nome;
    private $for_cnpj;
    private $for_rsocial;
    private $for_insc;
    private $for_logradouro;
    private $for_numero;
    private $for_cep;
    private $for_bairro;
    private $created_at;
    private $updated_at;
    private $deleted_at;
    private $cid_id;

    public function cidade()
    {
        $cidade = new Cidade();
        return $cidade->findWhere(array(['cid_id', $this->cid_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }


    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}