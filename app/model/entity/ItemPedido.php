<?php

namespace app\model\entity;

use system\Model;

class ItemPedido extends Model
{

    protected $table = 'estoque.itens_pedido';
    protected $primaryKey = 'iped_id';
    protected $fillable = ['prod_id', 'ped_id', 'iped_qtd', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.itens_pedido_iped_id_seq';
    private $iped_id;
    private $prod_id;
    private $ped_id;
    private $iped_qtd;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function produto()
    {
        $item = new Produto();
        return $item->findWhere(array(['prod_id', $this->prod_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }

}

