<?php

namespace app\model\entity;

use system\Model;

class ItemEntradaProduto extends Model
{

    protected $table = 'estoque.itens_entrada';
    protected $primaryKey = 'ient_id';
    protected $fillable = ['prod_id', 'ient_qtd','ent_id', 'ient_valor', 'ient_imposto', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.itens_entrada_ient_id_seq';
    private $ient_id;
    private $ient_qtd;
    private $ient_valor;
    private $ient_imposto;
    private $ent_id;
    private $prod_id;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function produto(){
        $item = new Produto();
        return $item->findWhere(array(['prod_id', $this->prod_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function entrada(){
        $item = new EntradaProduto();
        return $item->findWhere(array(['ent_id', $this->ent_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

