<?php

namespace app\model\entity;

use system\Model;

class PedidoCompra extends Model
{

    protected $table = 'estoque.pedidos';
    protected $primaryKey = 'ped_id';
    protected $fillable = ['usu_id', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.pedidos_ped_id_seq';
    private $ped_id;
    private $usu_id;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function usuario()
    {
        $fab = new Usuario();
        return $fab->findWhere(array(['usu_id', $this->usu_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function itemPedido()
    {
        $item = new ItemPedido();
        return $item->findWhere(array(['ped_id', $this->ped_id], ['deleted_at', 'IS', 'NULL']))->fetchAll();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

