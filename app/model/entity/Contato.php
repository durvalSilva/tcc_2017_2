<?php

namespace app\model\entity;

use system\Model;

class Contato extends Model
{

    protected $table = 'cadastro.contatos';
    protected $primaryKey = 'con_id';
    protected $fillable = ['con_nome','con_cargo','fab_id','for_id','con_fone', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.contatos_con_id_seq';
    private $con_id;
    private $con_nome;
    private $con_cargo;
    private $con_fone;
    private $created_at;
    private $updated_at;
    private $deleted_at;
    private $fab_id;
    private $for_id;


    public function fornecedor(){
        $for = new Fornecedor();
        return $for->findWhere(array(['for_id', $this->for_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function fabricante(){
        $fab = new Fabricante();
        return $fab->findWhere(array(['fab_id', $this->fab_id], ['deleted_at', 'IS', 'NULL']))->fetch();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }

}

