<?php

namespace app\model\entity;

use system\Model;

class EntradaProduto extends Model
{

    protected $table = 'estoque.entradas';
    protected $primaryKey = 'ent_id';
    protected $fillable = ['trans_id','usu_id', 'ent_total', 'ent_frete', 'ent_num_nf', 'ent_imposto', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.entradas_ent_id_seq';
    private $ent_id;
    private $usu_id;
    private $ent_total;
    private $ent_frete;
    private $ent_num_nf;
    private $trans_id;
    private $ent_imposto;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function itemEntrada()
    {
        $i = new ItemEntradaProduto();
        return $i->findWhere(array(['ent_id', $this->ent_id], ['deleted_at', 'IS', 'NULL']))->fetchAll();
    }

    public function transportadora()
    {
        $t = new Transportadora();
        return $t->findWhere(array(['trans_id', $this->trans_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function usuario()
    {
        $t = new Usuario();
        return $t->findWhere(array(['usu_id', $this->usu_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

