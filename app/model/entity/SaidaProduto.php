<?php

namespace app\model\entity;

use system\Model;

class SaidaProduto extends Model
{

    protected $table = 'estoque.saidas';
    protected $primaryKey = 'sai_id';
    protected $fillable = ['trans_id','usu_id', 'loj_id', 'sai_total', 'sai_frete', 'sai_imposto', 'created_at', 'updated_at'];
    protected $sequence = 'cadastro.fabricantes_fab_id_seq';
    private $sai_id;
    private $usu_id;
    private $trans_id;
    private $loj_id;
    private $sai_frete;
    private $sai_total;
    private $sai_imposto;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function transportadora()
    {
        $t = new Transportadora();
        return $t->findWhere(array(['trans_id', $this->trans_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function loja()
    {
        $l = new Loja();
        return $l->findWhere(array(['loj_id', $this->loj_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function ItemSaida(){
        $i = new ItemSaidaProduto();
        return $i->findWhere(array(['sai_id', $this->sai_id], ['deleted_at', 'IS', 'NULL']))->fetchAll();
    }

    public function usuario()
    {
        $t = new Usuario();
        return $t->findWhere(array(['usu_id', $this->usu_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

