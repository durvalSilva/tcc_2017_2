<?php

namespace app\model\entity;

use system\Model;

class Usuario extends Model
{

    protected $table = 'acesso.usuarios';
    protected $primaryKey = 'usu_id';
    protected $fillable = ['usu_nome','usu_email', 'usu_senha','tusu_id','created_at', 'updated_at'];
    protected $sequence = 'acesso.usuarios_usu_id_seq';
    private $usu_id;
    private $usu_nome;
    private $usu_email;
    private $usu_senha;
    private $tusu_id;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function tipoUsuario(){
        $uf = new TipoUsuario();
        return $uf->findWhere(array(['tusu_id', $this->tusu_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}