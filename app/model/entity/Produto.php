<?php

namespace app\model\entity;

use system\Model;

class Produto extends Model
{

    protected $table = 'estoque.produtos';
    protected $primaryKey = 'prod_id';
    protected $fillable = ['prod_nome', 'prod_descricao', 'prod_marca', 'prod_qtd_min', 'fab_id', 'pcat_id', 'created_at', 'updated_at'];
    protected $sequence = 'estoque.produtos_prod_id_seq';
    private $prod_id;
    private $prod_nome;
    private $prod_descricao;
    private $prod_marca;
    private $prod_qtd_min;
    private $fab_id;
    private $pcat_id;
    private $created_at;
    private $updated_at;
    private $deleted_at;


    public function fabricante()
    {
        $fab = new Fabricante();
        return $fab->findWhere(array(['fab_id', $this->fab_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function categoria()
    {
        $cat = new ProdutoCategoria();
        return $cat->findWhere(array(['pcat_id', $this->pcat_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}

