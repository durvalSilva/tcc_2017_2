<?php

namespace app\model\entity;

use system\Model;

class Uf extends Model
{
    protected $table = 'cadastro.uf';
    protected $primaryKey = 'uf_id';

    private $uf_id;
    private $uf_estado;
    private $uf_sigla;
    private $created_at;
    private $updated_at;
    private $deleted_at;

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        return $this->$name = $value;
    }

    public function create()
    {
        $dados = [];

        foreach ($this->fillable as $value) {
            if (!empty($this->$value)){
                $dados[$value] = $this->$value;
            }
        }

        $this->dados = $dados;
        $this->getSyntaxCreate();
        return $this->executeCreate();
    }
}