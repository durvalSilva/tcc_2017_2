<?php

namespace app\model\validator;

use system\Validator;

class UsuarioValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'usu_nome' => 'required',
            'usu_email' => 'required|email|unique:Usuario,usu_email',
            'usu_senha' => 'required',
            'tusu_id' => 'required|integer|exists:TipoUsuario,tusu_id',
        ], [
                'usu_nome.required' => 'O campo Nome é obrigatório.',
                'usu_email.required' => 'O campo E-mail é obrigatório.',
                'usu_email.email' => 'O campo E-mail não é valido.',
                'usu_email.unique' => 'O campo E-mail Já existe na base de dados.',
                'usu_senha.required' => 'O campo Senha é obrigatório.',
                'tusu_id.required' => 'O campo Tipo é obrigatório.',
                'tusu_id.exists' => 'Valor não existe na tabela Tipo de Usúario .',
                'tusu_id.integer' => 'Valor não existe na tabela Tipo de Usúario.',
            ]
        );
        return $result;
    }

    public function validacaoUpdate($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'usu_nome' => 'required',
            'usu_email' => 'required|email|uniqueUpdate:Usuario,usu_email',
            'usu_senha' => 'required',
            'tusu_id' => 'required|integer|exists:TipoUsuario,tusu_id',
        ], [
                'usu_nome.required' => 'O campo Nome é obrigatório.',
                'usu_email.required' => 'O campo E-mail é obrigatório.',
                'usu_email.email' => 'O campo E-mail não é valido.',
                'usu_email.unique' => 'O campo E-mail Já existe na base de dados.',
                'usu_senha.required' => 'O campo Senha é obrigatório.',
                'tusu_id.required' => 'O campo Tipo é obrigatório.',
                'tusu_id.exists' => 'Valor não existe na tabela Tipo de Usúario .',
                'tusu_id.integer' => 'Valor não existe na tabela Tipo de Usúario.',
            ]
        );
        return $result;
    }
}