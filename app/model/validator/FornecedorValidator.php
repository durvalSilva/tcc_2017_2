<?php

namespace app\model\validator;

use system\Validator;

class FornecedorValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'for_nome' => 'required',
            'for_rsocial' => 'required',
            'for_insc' => 'required',
            'for_logradouro' => 'required',
            'for_numero' => 'required',
            'for_cep' => 'required',
            'for_bairro' => 'required',
            'for_cnpj' => 'required',
            'cid_id' => 'required|integer|exists:cidade,cid_id',
        ], [
                'for_nome.required' => 'O campo Nome é obrigatório.',
                'for_rsocial.required' => 'O campo Nome é obrigatório.',
                'for_insc.required' => 'O campo Nome é obrigatório.',
                'for_logradouro.required' => 'O campo Logradouro é obrigatório.',
                'for_numero.required' => 'O campo Número é obrigatório.',
                'for_cep.required' => 'O campo CEP é obrigatório.',
                'for_bairro.required' => 'O campo Bairro é obrigatório.',
                'for_cnpj.required' => 'O campo CNPJ é obrigatório.',
                'cid_id.required' => 'O campo Cidade é obrigatório.',
                'cid_id.exists' => 'Valor não existe na tabela Cidades.',
                'cid_id.integer' => 'Valor não existe na tabela Cidades.',
            ]
        );
        return $result;
    }

}