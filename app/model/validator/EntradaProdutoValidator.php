<?php

namespace app\model\validator;

use system\Validator;

class EntradaprodutoValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'trans_id' => 'required|integer|exists:Transportadora,trans_id',
            'ent_frete' => 'required',
            'ent_num_nf' => 'required',
            'ent_imposto' => 'required',
            'ent_total' => 'required',
        ], [
                'ent_frete.required' => 'O campo Frete é obrigatório.',
                'ent_num_nf.required' => 'O campo Nota Fiscal é obrigatório.',
                'ent_imposto.required' => 'O campo Imposto é obrigatório.',
                'ent_total.required' => 'O campo Total é obrigatório.',
                'trans_id.required' => 'O campo Transportadora é obrigatório.',
                'trans_id.exists' => 'Valor não existe na tabela Transportadora.',
                'trans_id.integer' => 'Valor não existe na tabela Transportadora.',
            ]
        );
        return $result;
    }

}