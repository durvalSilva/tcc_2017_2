<?php

namespace app\model\validator;

use system\Validator;

class SaidaprodutoValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'trans_id' => 'required|integer|exists:Transportadora,trans_id',
            'loj_id' => 'required|integer|exists:Loja,loj_id',
            'sai_frete' => 'required',
            'sai_imposto' => 'required',
            'sai_total' => 'required',
        ], [
                'ent_frete.required' => 'O campo Frete é obrigatório.',
                'ent_imposto.required' => 'O campo Imposto é obrigatório.',
                'ent_total.required' => 'O campo Total é obrigatório.',
                'trans_id.required' => 'O campo Transportadora é obrigatório.',
                'trans_id.exists' => 'Valor não existe na tabela Transportadora.',
                'trans_id.integer' => 'Valor não existe na tabela Transportadora.',
                'loj_id.required' => 'O campo Loja é obrigatório.',
                'loj_id.exists' => 'Valor não existe na tabela Loja.',
                'loj_id.integer' => 'Valor não existe na tabela Loja.',
            ]
        );
        return $result;
    }

}