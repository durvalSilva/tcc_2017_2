<?php

namespace app\model\validator;

use system\Validator;

class LojaValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'loj_nome' => 'required',
            'loj_insc' => 'required',
            'loj_logradouro' => 'required',
            'loj_numero' => 'required',
            'loj_cep' => 'required',
            'loj_bairro' => 'required',
            'loj_cnpj' => 'required',
            'cid_id' => 'required|integer|exists:cidade,cid_id',
        ], [
                'loj_nome.required' => 'O campo Nome é obrigatório.',
                'loj_insc.required' => 'O campo Nome é obrigatório.',
                'loj_logradouro.required' => 'O campo Logradouro é obrigatório.',
                'loj_numero.required' => 'O campo Número é obrigatório.',
                'loj_cep.required' => 'O campo CEP é obrigatório.',
                'loj_bairro.required' => 'O campo Bairro é obrigatório.',
                'loj_cnpj.required' => 'O campo CNPJ é obrigatório.',
                'cid_id.required' => 'O campo Cidade é obrigatório.',
                'cid_id.exists' => 'Valor não existe na tabela Cidades.',
                'cid_id.integer' => 'Valor não existe na tabela Cidades.',
            ]
        );
        return $result;
    }

}