<?php

namespace app\model\validator;

use system\Validator;

class FabricanteValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'fab_nome' => 'required',
            'fab_logradouro' => 'required',
            'fab_numero' => 'required',
            'fab_cep' => 'required',
            'fab_bairro' => 'required',
            'fab_cnpj' => 'required',
            'cid_id' => 'required|integer|exists:cidade,cid_id',
        ], [
                'fab_nome.required' => 'O campo Nome é obrigatório.',
                'fab_logradouro.required' => 'O campo Logradouro é obrigatório.',
                'fab_numero.required' => 'O campo Número é obrigatório.',
                'fab_cep.required' => 'O campo CEP é obrigatório.',
                'fab_bairro.required' => 'O campo Bairro é obrigatório.',
                'fab_cnpj.required' => 'O campo CNPJ é obrigatório.',
                'cid_id.required' => 'O campo Cidade é obrigatório.',
                'cid_id.exists' => 'Valor não existe na tabela Cidades.',
                'cid_id.integer' => 'Valor não existe na tabela Cidades.',
            ]
        );
        return $result;
    }

}