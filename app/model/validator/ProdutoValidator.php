<?php

namespace app\model\validator;

use system\Validator;

class ProdutoValidator extends Validator
{

    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'prod_nome' => 'required',
            'prod_descricao' => 'required',
            'prod_marca' => 'required',
            'prod_qtd_min' => 'required|integer',
            'pcat_id' => 'required|integer|exists:ProdutoCategoria,pcat_id',
            'fab_id' => 'required|integer|exists:Fabricante,fab_id',
        ], [
                'prod_nome.required' => 'O campo Produto é obrigatório.',
                'prod_descricao.required' => 'O campo Descrição é obrigatório.',
                'prod_marca.required' => 'O campo Marca é obrigatório.',
                'prod_qtd_min.required' => 'O campo Quantidade minima é obrigatório.',
                'prod_qtd_min.integer' => 'O campo Quantidade minima deve ser numerico.',
                'pcat_id.required' => 'O campo Categoria é obrigatório.',
                'pcat_id.exists' => 'Valor não existe na tabela Categoria.',
                'pcat_id.integer' => 'Valor não existe na tabela Categoria.',
                'fab_id.required' => 'O campo Fabricante é obrigatório.',
                'fab_id.exists' => 'Valor não existe na tabela Fabricante.',
                'fab_id.integer' => 'Valor não existe na tabela Fabricante.',
            ]
        );
        return $result;
    }

}