<?php

namespace app\model\validator;

use system\Validator;

class TransportadoraValidator extends Validator
{

    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'trans_nome' => 'required',
            'trans_logradouro' => 'required',
            'trans_numero' => 'required',
            'trans_cep' => 'required',
            'trans_bairro' => 'required',
            'trans_cnpj' => 'required',
            'cid_id' => 'required|integer|exists:cidade,cid_id',
        ], [
                'trans_nome.required' => 'O campo Nome é obrigatório.',
                'trans_logradouro.required' => 'O campo Logradouro é obrigatório.',
                'trans_numero.required' => 'O campo Número é obrigatório.',
                'trans_cep.required' => 'O campo CEP é obrigatório.',
                'trans_bairro.required' => 'O campo Bairro é obrigatório.',
                'trans_cnpj.required' => 'O campo CNPJ é obrigatório.',
                'cid_id.required' => 'O campo Cidade é obrigatório.',
                'cid_id.exists' => 'Valor não existe na tabela Cidades.',
                'cid_id.integer' => 'Valor não existe na tabela Cidades.',
            ]
        );
        return $result;
    }

}