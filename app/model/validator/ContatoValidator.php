<?php

namespace app\model\validator;

use system\Validator;

class ContatoValidator extends Validator
{
    public function validacao($data)
    {
        $validator = new Validator();
        $result = $validator->validar($data, [
            'con_nome' => 'required',
            'con_cargo' => 'required',
            'con_fone' => 'required'
        ], [
                'con_nome.required' => 'O campo Nome é obrigatório.',
                'con_cargo.required' => 'O campo Cargo é obrigatório.',
                'con_fone.required' => 'O campo Fone é obrigatório.'
            ]
        );
        return $result;
    }

}