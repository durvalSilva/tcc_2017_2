<?php

namespace app\controller;

use app\model\entity\Fabricante;
use app\model\entity\Fornecedor;
use app\model\entity\ProdutoCategoria;
use app\model\service\AuthService;
use app\model\service\ProdutoService;

class ProdutoController extends AbstractController
{
    public $service;

    public function __construct()
    {
        $this->service = new ProdutoService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.produto.index', compact('data'));
    }

    public function create()
    {
        $fornecedor = new Fornecedor();
        $fabricante = new Fabricante();
        $categoria = new ProdutoCategoria();
        $fornecedores = $fornecedor->orderBy('for_nome', 'ASC')->all()->fetchAll(false);
        $fabricantes = $fabricante->orderBy('fab_nome', 'ASC')->all()->fetchAll(false);
        $categorias = $categoria->orderBy('pcat_nome', 'ASC')->all()->fetchAll(false);
        return $this->view('admin.produto.create', compact('fornecedores', 'fabricantes', 'categorias'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('produto/create'));
        } else {
            $data = (object)$data;
            $fornecedor = new Fornecedor();
            $fabricante = new Fabricante();
            $categoria = new ProdutoCategoria();
            $fornecedores = $fornecedor->orderBy('for_nome', 'ASC')->all()->fetchAll(false);
            $fabricantes = $fabricante->orderBy('fab_nome', 'ASC')->all()->fetchAll(false);
            $categorias = $categoria->orderBy('pcat_nome', 'ASC')->all()->fetchAll(false);
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.produto.create', compact('fornecedores', 'fabricantes', 'categorias','data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        $fabricante = new Fabricante();
        $categoria = new ProdutoCategoria();
        $fabricantes = $fabricante->orderBy('fab_nome', 'ASC')->all()->fetchAll(false);
        $categorias = $categoria->orderBy('pcat_nome', 'ASC')->all()->fetchAll(false);

        return $this->view('admin.produto.edit', compact('data','fornecedores', 'fabricantes', 'categorias'));
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('produto/edit/' . $id));
        } else {
            $data = (object)$data;
            $fabricante = new Fabricante();
            $categoria = new ProdutoCategoria();
            $fabricantes = $fabricante->orderBy('fab_nome', 'ASC')->all()->fetchAll(false);
            $categorias = $categoria->orderBy('pcat_nome', 'ASC')->all()->fetchAll(false);
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            $this->view('admin.produto.edit', compact('data','fornecedores', 'fabricantes', 'categorias'));
        }
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('produto'));
    }


}
