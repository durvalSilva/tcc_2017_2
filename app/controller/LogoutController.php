<?php

namespace app\controller;


class LogoutController
{

    public function index()
    {
        session_destroy();
        header('Location: home');
    }
}