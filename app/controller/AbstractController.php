<?php

namespace app\controller;

use System\Controller;

abstract class AbstractController extends Controller
{
    abstract public function index();

    abstract public function create();

    abstract public function store(array $data);

    abstract public function edit($id);

    abstract public function update($id, array $data);

    abstract public function delete($id);
}