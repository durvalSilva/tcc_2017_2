<?php

namespace app\controller;

use System\Controller;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        return $this->view('site.home');
    }

}