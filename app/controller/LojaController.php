<?php

namespace app\controller;

use app\model\entity\Cidade;
use app\model\entity\Uf;
use app\model\service\AuthService;
use app\model\service\LojaService;

class LojaController extends AbstractController
{
    public $service;

    public function __construct()
    {
        $this->service = new LojaService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.loja.index', compact('data'));
    }

    public function create()
    {
        $uf = new Uf();
        $cidades = new Cidade();
        $estados = $uf->all()->fetchAll();
        $cidades = $cidades->all()->fetchAll();
        return $this->view('admin.loja.create', compact('estados', 'cidades'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('loja/create'));
        } else {
            $data = (object)$data;
            $uf = new Uf();
            $cidades = new Cidade();
            $estados = $uf->all()->fetchAll();
            $cidades = $cidades->all()->fetchAll();
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.loja.create', compact('estados', 'cidades','data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        $uf = new Uf();
        $cidades = new Cidade();
        $estados = $uf->all()->fetchAll();
        $cidades = $cidades->all()->fetchAll();
        return $this->view('admin.loja.edit', compact('data', 'estados', 'cidades'));
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('loja/edit/' . $id));
        } else {
            $data = (object)$data;
            $uf = new Uf();
            $cidades = new Cidade();
            $estados = $uf->all()->fetchAll();
            $cidades = $cidades->all()->fetchAll();
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.loja.edit', compact('data', 'estados', 'cidades'));
        }
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('loja'));
    }

}

