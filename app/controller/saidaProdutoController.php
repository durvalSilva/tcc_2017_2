<?php

namespace app\controller;

use app\model\entity\Loja;
use app\model\entity\Transportadora;
use app\model\service\AuthService;
use app\model\service\ProdutoService;
use app\model\service\SaidaProdutoService;
use System\Controller;

class SaidaProdutoController extends Controller
{
    public $service;

    public function __construct()
    {
        $this->service = new SaidaProdutoService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {

        $data = $this->service->index();

        return $this->view('admin.produto.saida.index', compact('data'));
    }

    public function create()
    {
        $trans = new Transportadora();
        $produto = new ProdutoService();
        $loja = new Loja();
        $produtos = $produto->getProdutosInEstoque();
        $transportadoras = $trans->orderBy('trans_nome', 'ASC')->all()->fetchAll();
        $lojas = $loja->orderBy('loj_id','ASC')->all()->fetchAll();
        return $this->view('admin.produto.saida.create',compact('produtos','transportadoras','lojas'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('fabricante/create'));
        } else {
            $data = (object)$data;
            $trans = new Transportadora();
            $produto = new ProdutoService();
            $loja = new Loja();
            $produtos = $produto->getProdutosInEstoque();
            $transportadoras = $trans->orderBy('trans_nome', 'ASC')->all()->fetchAll();
            $lojas = $loja->orderBy('loj_id','ASC')->all()->fetchAll();
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return  $this->view('admin.produto.saida.create',compact('produtos','transportadoras','lojas','data'));

        }
    }


    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('fabricante'));
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.fabricante.edit' . $id);
        }
    }

    public function detalhes($id){
        $data = $this->service->edit($id);
        return $this->view('admin.produto.saida.visao',compact('data'));
    }

}
