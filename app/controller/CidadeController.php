<?php


namespace app\controller;

use app\model\entity\Cidade;
use app\model\service\AuthService;
use System\Controller;

class CidadeController extends Controller
{
    private $uf;
    private $cidade;

    public function __construct()
    {
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function uf($uf_id)
    {
        $this->cidade = new Cidade();
        $result = $this->cidade->orderBy('cid_nome', 'ASC')
            ->findWhere(array(['uf_id', $uf_id], ['deleted_at', 'IS', 'NULL']))
            ->fetchAll(false);
        $arr = array();
        foreach ($result as $item) {
            $cid = new \stdClass;
            $cid->cid_id = $item->cid_id;
            $cid->cid_nome = utf8_encode($item->cid_nome);
            array_push($arr, $cid);
        }

//        $result = json_encode(array_map(function ($item){
//           return array(
//               utf8_encode($item->cid_nome)
//           );
//        },$arr));
        echo json_encode($arr);
    }


}