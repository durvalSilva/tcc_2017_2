<?php


namespace app\controller;

use app\model\service\AuthService;
use app\model\service\ProdutoService;
use System\Controller;

class DashboardController extends Controller
{
    private $auth;

    public function __construct()
    {
        $this->auth = new AuthService();
        $this->auth->autorizar();

    }

    public function index()
    {
        $produto = new ProdutoService();
        $data = $produto->getProdutosInEstoque();
        return $this->view('admin.dashboard',compact('data'));
    }
}