<?php

namespace app\controller;

use app\model\entity\Produto;
use app\model\service\AuthService;
use app\model\service\PedidoCompraService;

class PedidoCompraController extends AbstractController
{
    public $service;

    public function __construct()
    {
        $this->service = new PedidoCompraService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.produto.pedido.index', compact('data'));
    }

    public function create()
    {
        $produto = new Produto();
        $produtos = $produto->orderBy('prod_nome', 'ASC')->all()->fetchAll();
        return $this->view('admin.produto.pedido.create', compact('produtos'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('pedidoCompra/create'));
        } else {
            return $this->view('admin.produto.pedido.create', compact('data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        $produto = new Produto();
        $produtos = $produto->orderBy('prod_nome', 'ASC')->all()->fetchAll();
        return $this->view('admin.produto.pedido.edit', compact('data', 'produtos'));
    }

    public function pedidojson($id)
    {
        $data = $this->service->pedidoJson($id);
        echo json_encode($data);
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('pedidoCompra/edit/' . $id));
        } else {
            return $this->view('admin.produto.pedido.edit' . $id, compact('data'));
        }
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('pedidoCompra'));
    }

}
