<?php

namespace app\controller;

use app\model\service\AuthService;
use app\model\service\ProdutoCategoriaService;

class ProdutoCategoriaController extends AbstractController
{
    public $service;

    public function __construct()
    {
        $this->service = new ProdutoCategoriaService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.produto.categoria.index', compact('data'));
    }

    public function create()
    {
        return $this->view('admin.produto.categoria.create');
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('produtoCategoria/create'));
        } else {
            return $this->view('admin.fabricante.create', compact('data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        return $this->view('admin.produto.categoria.edit', compact('data'));
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('produtoCategoria/edit/' . $id));
        } else {
            return $this->view('admin.produto.categoria.edit', compact('data'));
        }
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('produtoCategoria'));
    }

}
