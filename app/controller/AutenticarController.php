<?php

namespace app\controller;

use app\model\service\AuthService;

class AutenticarController
{
    public $service;

    public function __construct()
    {
        $this->service = new AuthService();
    }


    public function index($data)
    {
        $this->service->logar($data);
        header('Location: dashboard');
    }

}
