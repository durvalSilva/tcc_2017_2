<?php

namespace app\controller;

use app\model\entity\PedidoCompra;
use app\model\entity\Produto;
use app\model\entity\Transportadora;
use app\model\service\AuthService;
use app\model\service\EntradaProdutoService;
use System\Controller;

class EntradaProdutoController extends Controller
{
    public $service;

    public function __construct()
    {
        $this->service = new EntradaProdutoService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.produto.entrada.index', compact('data'));
    }

    public function create()
    {
        $trans = new Transportadora();
        $produto = new Produto();
        $produtos = $produto->orderBy('prod_nome','ASC')->all()->fetchAll();
        $transportadoras = $trans->orderBy('trans_nome', 'ASC')->all()->fetchAll();
        $pedidos = $this->service->getPedidos();
        return $this->view('admin.produto.entrada.create',compact('transportadoras','pedidos','produtos'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('entradaProduto/create'));
        } else {
            $data = (object)$data;
            $trans = new Transportadora();
            $produto = new Produto();
            $produtos = $produto->orderBy('prod_nome','ASC')->all()->fetchAll();
            $transportadoras = $trans->orderBy('trans_nome', 'ASC')->all()->fetchAll();
            $pedidos = $this->service->getPedidos();
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return  $this->view('admin.produto.entrada.create',compact('transportadoras','pedidos','produtos','data'));
        }
    }

    public function detalhes($id){
        $data = $this->service->edit($id);
        return $this->view('admin.produto.entrada.visao',compact('data'));
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('entradaProduto'));
    }

}
