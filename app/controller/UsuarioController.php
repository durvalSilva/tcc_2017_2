<?php

namespace app\controller;

use app\model\entity\TipoUsuario;
use app\model\service\UsuarioService;

class UsuarioController extends AbstractController
{
    public $service;

    public function __construct()
    {
        $this->service = new UsuarioService();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.usuario.index', compact('data'));
    }

    public function create()
    {
        if ($_SESSION['usuario']->tipo->tusu_id != 1) {
            header('Location:' . url_base('dashboard'));
        }
        $tipo = new TipoUsuario();
        $tipos = $tipo->orderBy('tusu_nome', 'ASC')->all()->fetchAll(false);
        return $this->view('admin.usuario.create', compact('tipos'));
    }

    public function store(array $data)
    {
        if ($_SESSION['usuario']->tipo->tusu_id != 1) {
            header('Location:' . url_base('dashboard'));
        }
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('usuario/create'));
        } else {
            $data = (object)$data;
            $tipo = new TipoUsuario();
            $tipos = $tipo->orderBy('tusu_nome', 'ASC')->all()->fetchAll(false);
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.usuario.create', compact('tipos','data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        $tipo = new TipoUsuario();
        $tipos = $tipo->orderBy('tusu_nome', 'ASC')->all()->fetchAll(false);
        return $this->view('admin.usuario.edit', compact('data', 'tipos'));
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('usuario/edit/' . $id));
        } else {
            $data = (object)$data;
            $tipo = new TipoUsuario();
            $tipos = $tipo->orderBy('tusu_nome', 'ASC')->all()->fetchAll(false);
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
            return $this->view('admin.usuario.edit', compact('data', 'tipos'));
        }
    }

    public function delete($id)
    {
        if ($_SESSION['usuario']->tipo->tusu_id != 1) {
            header('Location:' . url_base('dashboard'));
        }
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('usuario'));
    }

}
