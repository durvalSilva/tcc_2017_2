<?php

namespace app\controller;

use app\model\entity\Fabricante;
use app\model\entity\Fornecedor;
use app\model\service\AuthService;
use app\model\service\ContatoService;
use System\Controller;

class ContatoController extends Controller
{
    public $service;

    public function __construct()
    {
        $this->service = new ContatoService();
        $this->auth = new AuthService();
        $this->auth->autorizar();
    }

    public function index()
    {
        $data = $this->service->index();
        return $this->view('admin.contato.index', compact('data'));
    }

    public function store(array $data)
    {
        $result = $this->service->store($data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('contato/' . $result['redirect']));
        } else {
            return $this->view('admin.contato.create', compact('data'));
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);
        return $this->view('admin.contato.edit', compact('data'));
    }

    public function update($id, array $data)
    {
        $result = $this->service->update($id, $data);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
            header('Location:' . url_base('contato/edit/' . $id));
        } else {
            return $this->view('admin.contato.edit' . $id, compact('data'));
        }
    }

    public function delete($id)
    {
        $result = $this->service->delete($id);
        if ($result['status'] == 'sucesso') {
            $_SESSION['flashdata'] = array('status' => 'alert-success', 'mensagem' => $result['mensagem']);
        } else {
            $_SESSION['flashdata'] = array('status' => 'alert-danger', 'mensagem' => $result['mensagem']);
        }
        header('Location:' . url_base('contato'));
    }

    public function fornecedor($for_id)
    {
        $for = new Fornecedor();
        $for_id = $for->findWhere(array(['for_id', $for_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);

        return $this->view('admin.contato.fornecedor', compact('for_id'));
    }

    public function fabricante($fab_id)
    {
        $for = new Fabricante();
        $fab_id = $for->findWhere(array(['fab_id', $fab_id], ['deleted_at', 'IS', 'NULL']))->fetch(false);

        return $this->view('admin.contato.fabricante', compact('fab_id'));
    }

}

