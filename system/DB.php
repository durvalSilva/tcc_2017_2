<?php

namespace system;

class DB extends Config
{
    /** @var PDO */
    private static $connect = null;

    private static function Conectar()
    {
        try {
            if (empty(self::$connect)):
//                $options = [\PDO::ATTR_PERSISTENT => true];
                self::$connect = new \PDO('pgsql:host=' . self::host . ';port=' . self::port . ';dbname=' . self::dbname . ';user=' . self::user . ';password=' . self::password);
            endif;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die;
        }
        self::$connect->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return self::$connect;
    }

    protected static final function getConn()
    {
        return self::Conectar();
    }
}