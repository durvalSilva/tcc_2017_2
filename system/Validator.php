<?php

namespace system;


class Validator
{
    private $arrRegras;
    private $data;
    private $result;
    private $msg;
    private $retornoMsg;

    protected function validar(array $data, array $regras, array $msg)
    {
        $this->data = $data;
        $this->msg = $msg;
        foreach ($regras as $key => $value) {
            $this->arrRegras = explode("|", $value);
            foreach ($this->arrRegras as $item) {
                $this->validarRegra($key, $item);
            }
        }
        if (in_array(false, $this->result, true)) {
            return $this->retornoMsg;
        }
        return true;

    }

    private function validarRegra($key, $regra)
    {
        if (strpos($regra, ':') !== false) {
            $arr = explode(':', $regra);
            $regra = $arr[0];
            $ex = $arr[1];
            if (strpos($ex, ',') !== false) {
                $arr = explode(',', $ex);
                $entity = $arr[0];
                $column = $arr[1];
                $regra = '_' . $regra;
                $this->result[$key] = $this->$regra($key, $this->data[$key], $entity, $column);
            } else {
                $regra = '_' . $regra;
                $this->result[$key] = $this->$regra($key, $this->data[$key], $ex);
            }

        } else {
            $regra = '_' . $regra;
            $this->result[$key] = $this->$regra($key, $this->data[$key]);
        }

    }


    private function _required($key, $campo)
    {
        if (!empty($campo)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.required'];
            return false;
        }
    }

    private function _email($key, $campo)
    {
        if (filter_var($campo, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.email'];
            return false;
        }
    }


    private function _string($key, $campo)
    {
        if (!is_numeric($campo)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.string'];
            return false;
        }
    }

    private function _integer($key, $campo)
    {
        if (is_numeric($campo)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.integer'];
            return false;
        }
    }

    private function _min($key, $campo, $min)
    {
        if (count($campo) >= $min) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.min'];
            return false;
        }
    }

    private function _max($key, $campo, $max)
    {
        if (count($campo) <= $max) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.max'];
            return false;
        }
    }

    private function _unique($key, $campo, $entity, $column)
    {
        $entity = 'app\\model\\entity\\' . ucfirst($entity);
        $class = new $entity;
        $result = $class->findWhere(array([$column, $campo], ['deleted_at', 'IS', 'NULL']))->fetch();
        if (empty($result)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.unique'];
            return false;
        }

    }


    private function _uniqueUpdate($key, $campo, $entity, $column)
    {
        $entity = 'app\\model\\entity\\' . ucfirst($entity);
        $class = new $entity;
        $result = $class->findWhere(array([$column, $campo], ['deleted_at', 'IS', 'NULL'],[$key,'!=',$campo]))->fetch();
        if (empty($result)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.unique'];
            return false;
        }

    }

    private function _exists($key, $campo, $entity, $column)
    {
        $entity = 'app\\model\\entity\\' . ucfirst($entity);
        $class = new $entity;
        $result = $class->findWhere(array([$column, $campo], ['deleted_at', 'IS', 'NULL']))->fetch();
        if (!empty($result)) {
            return true;
        } else {
            $this->retornoMsg[$key] = $this->msg[$key . '.exists'];
            return false;
        }
    }
}