<?php

namespace system;

/**
 * Class System
 * @package system
 * fazer com que se o controller não existir apontar pra o controlle raiz
 * fazer com que se a action não existir apontar pra a action padrão
 */
class System
{
    private $url;
    private $explode;
    private $controller;
    private $action;
    private $params;
    private $pathController;
    private $runController;
    private $paramsValue = array();
    private $paramsKey = array('id');

    public function __construct()
    {
        $this->setUrl();
        $this->setExploder();
        $this->setController();
        $this->setAction();
        $this->setParams();
    }

    private function setUrl()
    {
        $this->url = (isset($_GET['url']) ? $_GET['url'] : 'home/index');
    }

    private function setExploder()
    {
        $this->explode = explode('/', $this->url);

    }


    private function setController()
    {
        $this->controller = (isset($this->explode[0])) ? $this->explode[0] : 'home';
    }

    private function setAction()
    {
        $this->action = (!empty($this->explode[1])) ? $this->explode[1] : 'index';
    }

    private function setParams()
    {
        unset($this->explode[0], $this->explode[1]);
        if (end($this->explode) == null) {
            array_pop($this->explode);
        }
        if (empty($this->explode)) {
            $this->params = array();
        } else {
            if (count($this->explode) == 1) {
                $this->paramsValue[] = $this->explode[2];
            } else {
                $i = 0;
                foreach ($this->explode as $item) {
                    if ($i % 2 == 0) {
                        $this->paramsValue[] = $item;
                    } else {
                        $this->paramsKey[] = $item;
                    }
                    $i++;
                }
                if (count($this->paramsValue) > count($this->paramsKey)) {
                    array_pop($paramsValue);
                } else if (count($this->paramsValue) < count($this->paramsKey)) {
                    array_pop($paramsKey);
                }
            }
            $this->params = array_combine($this->paramsKey, $this->paramsValue);
        }
    }

    private function validateController()
    {
        if (!class_exists($this->pathController)) {
            die($this->pathController);
            header('Location: dashboard');
//            trigger_error("Classe não existe: $this->controller", E_USER_WARNING);
            die();
        } else {
            $this->validateAction();
        }
    }

    private function validateAction()
    {
        if (!method_exists($this->pathController, $this->action)) {
            trigger_error("Método não existe: $this->action", E_USER_WARNING);
            die();
        }
    }


    protected function getParam($key)
    {
        return (isset($this->params[$key])) ? $this->params[$key] : null;
    }

    protected function getAllParam()
    {
        return $this->params;
    }

//    public function __toString()
//    {
//        $object = array();
//        foreach ($this as $key => $value) {
//            $object[$key] = $value;
//        }
//        return json_encode($object);
//    }

    public function run()
    {
        $this->pathController = "app\\controller\\" . ucfirst($this->controller) . "Controller";

        $this->validateController();
        $this->runController = new $this->pathController();
        $act = $this->action;
        if (isset($this->params['id']) && empty($_POST) && count($this->params) == 1) {
            $this->runController->$act($this->params['id']);
        } else if (!empty($_POST) && $act == 'update' && ucfirst($this->controller) != 'autenticar') {
            $this->runController->$act($this->params['id'], $_POST);
        } else if (!empty($_POST) && $act == 'store') {
            $this->runController->$act($_POST);
        } else if (!empty($_POST) && $act == 'index' && $this->controller == 'autenticar') {
            $this->runController->$act($_POST);
        } else if (count($this->params) > 1) {
            $this->runController->$act(...$this->paramsValue);
        } else {
            $this->runController->$act();
        }
    }
}