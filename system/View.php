<?php

namespace system;

class View
{
    private $view;
    private $file;
    private $explode;
    private $ext = '.php';
    private $pathView = "public/view/";

//    private $pathAssets = "public/assets/";

    public function __construct($view)
    {
        $this->exploder($view);
        $this->setView();
        $this->setFile();
    }

    private function exploder($view)
    {
        $this->explode = explode('.', $view);
    }

    private function setView()
    {
        $arrView = $this->explode;
        array_pop($arrView);
        foreach ($arrView as $item) {
            if (is_dir($this->pathView . $this->view . $item)) {
                $this->view .= $item . '/';
            } else {
                trigger_error("Pasta não existe: {$this->explode[0]}", E_USER_WARNING);
                die();
            }
        }

    }

    private function setFile()
    {
        if (file_exists($this->pathView . $this->view . end($this->explode) . $this->ext)) {
            $this->file = end($this->explode) . $this->ext;
        } else {
            trigger_error("Arquivo não existe: {$this->explode[0]}", E_USER_WARNING);
            die();
        }
    }

    public function getView()
    {
        return $this->pathView . $this->view . $this->file;
    }

}