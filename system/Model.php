<?php

namespace system;

abstract class Model extends DB
{
    protected $table = '';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $softDelete = true;
    protected $collumSoftDelete = 'deleted_at';
    protected $sequence = '';
    /**
     * @var \PDO|PDO
     */
    private $db;
    private $sql;
    protected $dados;
    private $dadosFillable;
    private $_where;
    private $_orderBy = '';
    private $collumUpdate = 'updated_at';

    public function __construct()
    {
        $this->db = self::getConn();
    }

    protected function getSyntaxCreate()
    {
        $this->dadosFillable = array_intersect_key($this->dados, array_flip($this->fillable));
        $this->dadosFillable[$this->primaryKey] = $this->getIDSequence();
        $filds = implode(', ', array_keys($this->dadosFillable));
        $places = ':' . implode(', :', array_keys($this->dadosFillable));
        $this->sql = $this->db->prepare("INSERT INTO {$this->table} ({$filds}) VALUES ({$places})");

        foreach ($this->dadosFillable as $key => $value) {
            $this->bind(":" . $key, $value);
        }
    }


    private function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }
        $this->sql->bindValue($param, $value, $type);
    }


    private function getIDSequence()
    {
        return $this->db->query("SELECT nextval('" . $this->sequence . "')")->fetch(\PDO::FETCH_OBJ)->nextval;
    }

    /**
     * executa a query! e retorna o objeto inserido
     */
    protected function executeCreate()
    {
//        try {
        $this->sql->execute();
        return $this->find($this->dadosFillable[$this->primaryKey])->fetch();
//        } catch (\PDOException $e) {
//            return ($e->getMessage() . ' line ' . $e->getLine());
//        }
    }

    private function createWhere(array $item)
    {
        if (count($item) == 3) {
            if (strtoupper($item[2]) != 'NULL') {
                $item[2] = "'" . $item[2] . "'";
            }
            $this->_where[] = implode(' ', $item);
        } else {
            $this->_where[] = $item[0] . " = '" . $item[1] . "'";
        }
    }

    public function customQuery($sql, $select = true)
    {
        if ($select == true) {
            return $this->db->query($sql)->fetchAll(\PDO::FETCH_OBJ);
        } else {
//            $this->sql = $this->db->prepare($sql);
//            return $this->sql->execute();
            return $this->db->exec($sql);
        }
    }


    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollback()
    {
        $this->db->rollback();
    }

    /**
     * @param array $attributes
     * @param ARRAY $attributes = Informe um array chave valor. ( Nome Da Coluna => Valor ).
     */
//    public function create()
//    {
//        $dados = [];
//
//        foreach ($this->fillable as $value) {
//            if (!empty($this->$value))
//                $dados[$value] = $this->$value;
//        }
//
//        $this->dados = $dados;
//        $this->getSyntaxCreate();
//        return $this->executeCreate();
//    }

    abstract function create();

    public function update(array $attributes, $id)
    {

        foreach ($attributes as $key => $value) {
            if (is_string($value)) {
                $places[] = $key . " = '" . $value . "'";
            } else if (is_int($value)) {
                $places[] = $key . ' = ' . $value;
            }
        }
        $places[] = $this->collumUpdate . ' = NOW()';
        $places = implode(', ', $places);
        $this->sql = "UPDATE {$this->table}	SET {$places} WHERE {$this->primaryKey} = {$id}";
        return $this->db->exec($this->sql);
    }

    public function delete($id)
    {
        if ($this->softDelete == true) {
            $this->update([$this->collumSoftDelete => 'NOW()'], $id);
        } else if (!empty($this->_where)) {
            $this->sql = "DELETE FROM {$this->table} WHERE {$this->primaryKey} = {$id} AND {$this->_where}";
        } else {
            $this->sql = "DELETE FROM {$this->table} WHERE {$this->primaryKey} = {$id}";
        }
        return $this->db->exec($this->sql);
    }

    public function find($id, $columns = ['*'])
    {
        $columns = implode(',', $columns);
        $this->sql = "SELECT {$columns} FROM {$this->table} WHERE {$this->primaryKey} = {$id}";
        $this->sql = $this->db->query($this->sql);
        return $this;
    }

    public function findWhere(array $where, $columns = ['*'])
    {
        $columns = implode(',', $columns);
        $this->where($where);
        $this->_where = implode(' AND ', $this->_where);
        $this->sql = "SELECT {$columns} FROM {$this->table} WHERE {$this->_where} {$this->_orderBy}";
        $this->sql = $this->db->query($this->sql);
        return $this;

    }

    public function orderBy($column, $direction = 'asc')
    {
        $this->_orderBy = " ORDER BY {$column} {$direction}";
        return $this;
    }

    public function all($columns = null)
    {
        if (empty($columns))
            $columns = array('*');
        $columns = implode(',', $columns);
        $this->sql = "SELECT {$columns} FROM {$this->table} WHERE {$this->collumSoftDelete} IS NULL {$this->_orderBy}";
        $this->sql = $this->db->query($this->sql);
        return $this;
    }

    public function findByField($field, $value, $columns = ['*'])
    {
        $columns = implode(',', $columns);
        $this->sql = "SELECT {$columns} FROM {$this->table} WHERE {$field} = '{$value}'";
        $this->sql = $this->db->query($this->sql);
        return $this;

    }

    public function where(array $where)
    {
        if (is_array($where[0])) {
            foreach ($where as $item) {
                $this->createWhere($item);
            }
        } else {
            $this->createWhere($where);
        }

        return $this;
    }

    public function fetchAll($boolean = true)
    {
        $result = $this->sql->fetchAll(\PDO::FETCH_OBJ);
        if (!empty($result)) {
            if ($boolean) {
                $class = get_class($this);
                $arr = array();
                foreach ($result as $item) {
                    $obj = new Objeto($class, $item);
                    array_push($arr, $obj->setPropriedade());
                }
            } else {
                $arr = $result;
            }
            return $arr;
        } else {
            return array();
        }
    }

    public function fetch($boolean = true)
    {
        $result = $this->sql->fetch(\PDO::FETCH_OBJ);
        if (!empty($result)) {
            if ($boolean) {
                $class = get_class($this);
                $obj = new Objeto($class, $result);
                $arr = $obj->setPropriedade();
            } else {
                $arr = $result;
            }
            return $arr;
        } else {
            return array();
        }
    }


}