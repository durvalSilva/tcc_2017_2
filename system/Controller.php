<?php

/**
 * @author: Edval Silva
 * @version 0.1
 * Classe base para todos Controller
 */
namespace System;

abstract class Controller extends System
{
    protected $title = "TCC 2017";
    protected $description;
    protected $author;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param $view
     * @param $params
     * composição
     */
    protected function view($view, $params = null)
    {
        $view = new \system\View($view);
        if ($params != null)
            extract($params);
        require_once($view->getView());
    }

}